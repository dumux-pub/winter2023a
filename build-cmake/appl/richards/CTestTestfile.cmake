# CMake generated Testfile for 
# Source directory: /home/rwinter/DUMUX/winter2023a/appl/richards
# Build directory: /home/rwinter/DUMUX/winter2023a/build-cmake/appl/richards
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_ri_tpfa "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/richards/test_ri_tpfa")
set_tests_properties(test_ri_tpfa PROPERTIES  LABELS "porousmediumflow;richards" PROCESSORS "1" REQUIRED_FILES "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/richards/test_ri_tpfa" SKIP_RETURN_CODE "77" TIMEOUT "300" WORKING_DIRECTORY "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/richards" _BACKTRACE_TRIPLES "/home/rwinter/DUMUX/dune-common/cmake/modules/DuneTestMacros.cmake;414;_add_test;/home/rwinter/DUMUX/winter2023a/appl/richards/CMakeLists.txt;3;dune_add_test;/home/rwinter/DUMUX/winter2023a/appl/richards/CMakeLists.txt;0;")
