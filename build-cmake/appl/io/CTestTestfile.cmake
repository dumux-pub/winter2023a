# CMake generated Testfile for 
# Source directory: /home/rwinter/DUMUX/winter2023a/appl/io
# Build directory: /home/rwinter/DUMUX/winter2023a/build-cmake/appl/io
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_gridmanager_column "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/io/test_gridmanager_column")
set_tests_properties(test_gridmanager_column PROPERTIES  LABELS "unit;io" PROCESSORS "1" REQUIRED_FILES "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/io/test_gridmanager_column" SKIP_RETURN_CODE "77" TIMEOUT "300" WORKING_DIRECTORY "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/io" _BACKTRACE_TRIPLES "/home/rwinter/DUMUX/dune-common/cmake/modules/DuneTestMacros.cmake;414;_add_test;/home/rwinter/DUMUX/dumux/cmake/modules/DumuxTestMacros.cmake;210;dune_add_test;/home/rwinter/DUMUX/winter2023a/appl/io/CMakeLists.txt;3;dumux_add_test;/home/rwinter/DUMUX/winter2023a/appl/io/CMakeLists.txt;0;")
add_test(test_gridmanager_landfill "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/io/test_gridmanager_landfill")
set_tests_properties(test_gridmanager_landfill PROPERTIES  LABELS "unit;io" PROCESSORS "1" REQUIRED_FILES "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/io/test_gridmanager_landfill" SKIP_RETURN_CODE "77" TIMEOUT "300" WORKING_DIRECTORY "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/io" _BACKTRACE_TRIPLES "/home/rwinter/DUMUX/dune-common/cmake/modules/DuneTestMacros.cmake;414;_add_test;/home/rwinter/DUMUX/dumux/cmake/modules/DumuxTestMacros.cmake;210;dune_add_test;/home/rwinter/DUMUX/winter2023a/appl/io/CMakeLists.txt;7;dumux_add_test;/home/rwinter/DUMUX/winter2023a/appl/io/CMakeLists.txt;0;")
