# CMake generated Testfile for 
# Source directory: /home/rwinter/DUMUX/winter2023a/appl
# Build directory: /home/rwinter/DUMUX/winter2023a/build-cmake/appl
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("2p")
subdirs("2p2cevaporation")
subdirs("io")
subdirs("richards")
subdirs("tracer")
