# CMake generated Testfile for 
# Source directory: /home/rwinter/DUMUX/winter2023a/appl/2p
# Build directory: /home/rwinter/DUMUX/winter2023a/build-cmake/appl/2p
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_2p_tpfa "/home/rwinter/DUMUX/winter2023a/bin/testing/runtest.py")
set_tests_properties(test_2p_tpfa PROPERTIES  LABELS "porousmediumflow;2p" PROCESSORS "1" REQUIRED_FILES "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/2p/test_2p_tpfa" SKIP_RETURN_CODE "77" TIMEOUT "300" WORKING_DIRECTORY "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/2p" _BACKTRACE_TRIPLES "/home/rwinter/DUMUX/dune-common/cmake/modules/DuneTestMacros.cmake;414;_add_test;/home/rwinter/DUMUX/winter2023a/appl/2p/CMakeLists.txt;3;dune_add_test;/home/rwinter/DUMUX/winter2023a/appl/2p/CMakeLists.txt;0;")
add_test(test_2p_forch_const_tpfa "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/2p/test_2p_forch_const_tpfa")
set_tests_properties(test_2p_forch_const_tpfa PROPERTIES  LABELS "porousmediumflow;2p" PROCESSORS "1" REQUIRED_FILES "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/2p/test_2p_forch_const_tpfa" SKIP_RETURN_CODE "77" TIMEOUT "300" WORKING_DIRECTORY "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/2p" _BACKTRACE_TRIPLES "/home/rwinter/DUMUX/dune-common/cmake/modules/DuneTestMacros.cmake;414;_add_test;/home/rwinter/DUMUX/dumux/cmake/modules/DumuxTestMacros.cmake;210;dune_add_test;/home/rwinter/DUMUX/winter2023a/appl/2p/CMakeLists.txt;9;dumux_add_test;/home/rwinter/DUMUX/winter2023a/appl/2p/CMakeLists.txt;0;")
add_test(test_2p_forch_mp_tpfa "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/2p/test_2p_forch_mp_tpfa")
set_tests_properties(test_2p_forch_mp_tpfa PROPERTIES  LABELS "porousmediumflow;2p" PROCESSORS "1" REQUIRED_FILES "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/2p/test_2p_forch_mp_tpfa" SKIP_RETURN_CODE "77" TIMEOUT "300" WORKING_DIRECTORY "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/2p" _BACKTRACE_TRIPLES "/home/rwinter/DUMUX/dune-common/cmake/modules/DuneTestMacros.cmake;414;_add_test;/home/rwinter/DUMUX/dumux/cmake/modules/DumuxTestMacros.cmake;210;dune_add_test;/home/rwinter/DUMUX/winter2023a/appl/2p/CMakeLists.txt;15;dumux_add_test;/home/rwinter/DUMUX/winter2023a/appl/2p/CMakeLists.txt;0;")
