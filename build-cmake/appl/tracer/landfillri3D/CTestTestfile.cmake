# CMake generated Testfile for 
# Source directory: /home/rwinter/DUMUX/winter2023a/appl/tracer/landfillri3D
# Build directory: /home/rwinter/DUMUX/winter2023a/build-cmake/appl/tracer/landfillri3D
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_lfri3D_tpfa "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/tracer/landfillri3D/test_lfri3D_tpfa")
set_tests_properties(test_lfri3D_tpfa PROPERTIES  LABELS "porousmediumflow;richards;tracer" PROCESSORS "1" REQUIRED_FILES "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/tracer/landfillri3D/test_lfri3D_tpfa" SKIP_RETURN_CODE "77" TIMEOUT "300" WORKING_DIRECTORY "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/tracer/landfillri3D" _BACKTRACE_TRIPLES "/home/rwinter/DUMUX/dune-common/cmake/modules/DuneTestMacros.cmake;414;_add_test;/home/rwinter/DUMUX/dumux/cmake/modules/DumuxTestMacros.cmake;210;dune_add_test;/home/rwinter/DUMUX/winter2023a/appl/tracer/landfillri3D/CMakeLists.txt;3;dumux_add_test;/home/rwinter/DUMUX/winter2023a/appl/tracer/landfillri3D/CMakeLists.txt;0;")
