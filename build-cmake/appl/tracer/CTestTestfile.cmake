# CMake generated Testfile for 
# Source directory: /home/rwinter/DUMUX/winter2023a/appl/tracer
# Build directory: /home/rwinter/DUMUX/winter2023a/build-cmake/appl/tracer
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("comp_hydrus")
subdirs("decay")
subdirs("landfillri")
subdirs("landfillri3D")
