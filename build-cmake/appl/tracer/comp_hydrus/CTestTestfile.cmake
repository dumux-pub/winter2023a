# CMake generated Testfile for 
# Source directory: /home/rwinter/DUMUX/winter2023a/appl/tracer/comp_hydrus
# Build directory: /home/rwinter/DUMUX/winter2023a/build-cmake/appl/tracer/comp_hydrus
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_comp_hydrus "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/tracer/comp_hydrus/test_comp_hydrus")
set_tests_properties(test_comp_hydrus PROPERTIES  LABELS "porousmediumflow;richards;tracer" PROCESSORS "1" REQUIRED_FILES "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/tracer/comp_hydrus/test_comp_hydrus" SKIP_RETURN_CODE "77" TIMEOUT "300" WORKING_DIRECTORY "/home/rwinter/DUMUX/winter2023a/build-cmake/appl/tracer/comp_hydrus" _BACKTRACE_TRIPLES "/home/rwinter/DUMUX/dune-common/cmake/modules/DuneTestMacros.cmake;414;_add_test;/home/rwinter/DUMUX/dumux/cmake/modules/DumuxTestMacros.cmake;210;dune_add_test;/home/rwinter/DUMUX/winter2023a/appl/tracer/comp_hydrus/CMakeLists.txt;3;dumux_add_test;/home/rwinter/DUMUX/winter2023a/appl/tracer/comp_hydrus/CMakeLists.txt;0;")
