if(NOT winter2023a_FOUND)
# Whether this module is installed or not
set(winter2023a_INSTALLED OFF)

# Settings specific to the module

# Package initialization
# Set prefix to source dir
set(PACKAGE_PREFIX_DIR /home/rwinter/DUMUX/winter2023a)
macro(set_and_check _var _file)
  set(${_var} "${_file}")
  if(NOT EXISTS "${_file}")
    message(FATAL_ERROR "File or directory ${_file} referenced by variable ${_var} does not exist !")
  endif()
endmacro()

#report other information
set_and_check(winter2023a_PREFIX "${PACKAGE_PREFIX_DIR}")
set_and_check(winter2023a_INCLUDE_DIRS "/home/rwinter/DUMUX/winter2023a")
set(winter2023a_CXX_FLAGS "-std=c++17 ")
set(winter2023a_CXX_FLAGS_DEBUG "-O0 -g -ggdb -Wall -Wextra -Wno-unused-parameter -Wno-sign-compare")
set(winter2023a_CXX_FLAGS_MINSIZEREL "-Os -DNDEBUG")
set(winter2023a_CXX_FLAGS_RELEASE " -fdiagnostics-color=always -fno-strict-aliasing -fstrict-overflow -fno-finite-math-only -DNDEBUG=1 -O3 -march=native -funroll-loops -g0 -Wall -Wunused -Wmissing-include-dirs -Wcast-align -Wno-missing-braces -Wmissing-field-initializers -Wno-sign-compare")
set(winter2023a_CXX_FLAGS_RELWITHDEBINFO " -fdiagnostics-color=always -fno-strict-aliasing -fstrict-overflow -fno-finite-math-only -DNDEBUG=1 -O3 -march=native -funroll-loops -g0 -Wall -Wunused -Wmissing-include-dirs -Wcast-align -Wno-missing-braces -Wmissing-field-initializers -Wno-sign-compare -g -ggdb -Wall")
set(winter2023a_DEPENDS "dumux")
set(winter2023a_SUGGESTS "")
set(winter2023a_MODULE_PATH "/home/rwinter/DUMUX/winter2023a/cmake/modules")
set(winter2023a_LIBRARIES "")
set(winter2023a_HASPYTHON 0)
set(winter2023a_PYTHONREQUIRES "")

# Lines that are set by the CMake build system via the variable DUNE_CUSTOM_PKG_CONFIG_SECTION


#import the target
if(winter2023a_LIBRARIES)
  get_filename_component(_dir "${CMAKE_CURRENT_LIST_FILE}" PATH)
  include("${_dir}/winter2023a-targets.cmake")
endif()

endif()
