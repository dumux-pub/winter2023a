# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.27

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /snap/cmake/1356/bin/cmake

# The command to remove a file.
RM = /snap/cmake/1356/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/rwinter/DUMUX/winter2023a

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/rwinter/DUMUX/winter2023a/build-cmake

# Utility rule file for doxygen_winter2023a.

# Include any custom commands dependencies for this target.
include doc/doxygen/CMakeFiles/doxygen_winter2023a.dir/compiler_depend.make

# Include the progress variables for this target.
include doc/doxygen/CMakeFiles/doxygen_winter2023a.dir/progress.make

doc/doxygen/CMakeFiles/doxygen_winter2023a: doc/doxygen/html

doc/doxygen/html: doc/doxygen/Doxyfile.in
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --blue --bold --progress-dir=/home/rwinter/DUMUX/winter2023a/build-cmake/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building doxygen documentation. This may take a while"
	cd /home/rwinter/DUMUX/winter2023a/build-cmake/doc/doxygen && /snap/cmake/1356/bin/cmake -D DOXYGEN_EXECUTABLE=/usr/bin/doxygen -P /home/rwinter/DUMUX/dune-common/cmake/scripts/RunDoxygen.cmake

doc/doxygen/Doxyfile.in: /home/rwinter/DUMUX/dune-common/doc/doxygen/Doxystyle
doc/doxygen/Doxyfile.in: /home/rwinter/DUMUX/dune-common/doc/doxygen/doxygen-macros
doc/doxygen/Doxyfile.in: /home/rwinter/DUMUX/winter2023a/doc/doxygen/Doxylocal
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --blue --bold --progress-dir=/home/rwinter/DUMUX/winter2023a/build-cmake/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Creating Doxyfile.in"
	cd /home/rwinter/DUMUX/winter2023a/build-cmake/doc/doxygen && /snap/cmake/1356/bin/cmake -D DOT_TRUE= -D DUNE_MOD_NAME=winter2023a -D DUNE_MOD_VERSION=1.0 -D DOXYSTYLE=/home/rwinter/DUMUX/dune-common/doc/doxygen/Doxystyle -D DOXYGENMACROS=/home/rwinter/DUMUX/dune-common/doc/doxygen/doxygen-macros -D DOXYLOCAL=/home/rwinter/DUMUX/winter2023a/doc/doxygen/Doxylocal -D abs_top_srcdir=/home/rwinter/DUMUX/winter2023a -D srcdir=/home/rwinter/DUMUX/winter2023a/doc/doxygen -D top_srcdir=/home/rwinter/DUMUX/winter2023a -P /home/rwinter/DUMUX/dune-common/cmake/scripts/CreateDoxyFile.cmake

doc/doxygen/Doxyfile: doc/doxygen/Doxyfile.in
	@$(CMAKE_COMMAND) -E touch_nocreate doc/doxygen/Doxyfile

doxygen_winter2023a: doc/doxygen/CMakeFiles/doxygen_winter2023a
doxygen_winter2023a: doc/doxygen/Doxyfile
doxygen_winter2023a: doc/doxygen/Doxyfile.in
doxygen_winter2023a: doc/doxygen/html
doxygen_winter2023a: doc/doxygen/CMakeFiles/doxygen_winter2023a.dir/build.make
.PHONY : doxygen_winter2023a

# Rule to build all files generated by this target.
doc/doxygen/CMakeFiles/doxygen_winter2023a.dir/build: doxygen_winter2023a
.PHONY : doc/doxygen/CMakeFiles/doxygen_winter2023a.dir/build

doc/doxygen/CMakeFiles/doxygen_winter2023a.dir/clean:
	cd /home/rwinter/DUMUX/winter2023a/build-cmake/doc/doxygen && $(CMAKE_COMMAND) -P CMakeFiles/doxygen_winter2023a.dir/cmake_clean.cmake
.PHONY : doc/doxygen/CMakeFiles/doxygen_winter2023a.dir/clean

doc/doxygen/CMakeFiles/doxygen_winter2023a.dir/depend:
	cd /home/rwinter/DUMUX/winter2023a/build-cmake && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/rwinter/DUMUX/winter2023a /home/rwinter/DUMUX/winter2023a/doc/doxygen /home/rwinter/DUMUX/winter2023a/build-cmake /home/rwinter/DUMUX/winter2023a/build-cmake/doc/doxygen /home/rwinter/DUMUX/winter2023a/build-cmake/doc/doxygen/CMakeFiles/doxygen_winter2023a.dir/DependInfo.cmake "--color=$(COLOR)"
.PHONY : doc/doxygen/CMakeFiles/doxygen_winter2023a.dir/depend

