# Install script for directory: /home/rwinter/DUMUX/winter2023a/doc/doxygen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process(COMMAND /snap/cmake/1356/bin/cmake --build /home/rwinter/DUMUX/winter2023a/build-cmake --target doxygen_winter2023a
          WORKING_DIRECTORY /home/rwinter/DUMUX/winter2023a/build-cmake/doc/doxygen)
        file(GLOB doxygenfiles
          GLOB /home/rwinter/DUMUX/winter2023a/build-cmake/doc/doxygen/html/*.html
          /home/rwinter/DUMUX/winter2023a/build-cmake/doc/doxygen/html/*.js
          /home/rwinter/DUMUX/winter2023a/build-cmake/doc/doxygen/html/*.png
          /home/rwinter/DUMUX/winter2023a/build-cmake/doc/doxygen/html/*.css
          /home/rwinter/DUMUX/winter2023a/build-cmake/doc/doxygen/html/*.gif
          /home/rwinter/DUMUX/winter2023a/build-cmake/doc/doxygen/*.tag
          )
        set(doxygenfiles "${doxygenfiles}")
        foreach(_file ${doxygenfiles})
           get_filename_component(_basename ${_file} NAME)
           LIST(APPEND CMAKE_INSTALL_MANIFEST_FILES /usr/local/share/doc/winter2023a/doxygen/${_basename})
         endforeach()
         file(INSTALL ${doxygenfiles} DESTINATION /usr/local/share/doc/winter2023a/doxygen)
         message(STATUS "Installed doxygen into /usr/local/share/doc/winter2023a/doxygen")
endif()

