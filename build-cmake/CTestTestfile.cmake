# CMake generated Testfile for 
# Source directory: /home/rwinter/DUMUX/winter2023a
# Build directory: /home/rwinter/DUMUX/winter2023a/build-cmake
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("appl")
subdirs("src")
subdirs("dumux")
subdirs("dune")
subdirs("doc")
subdirs("cmake/modules")
