# CMake generated Testfile for 
# Source directory: /home/rwinter/DUMUX/winter2023a/dumux
# Build directory: /home/rwinter/DUMUX/winter2023a/build-cmake/dumux
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("common")
subdirs("flux")
subdirs("io")
subdirs("material")
subdirs("porousmediumflow")
