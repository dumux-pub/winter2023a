# CMake generated Testfile for 
# Source directory: /home/rwinter/DUMUX/winter2023a/dumux/porousmediumflow
# Build directory: /home/rwinter/DUMUX/winter2023a/build-cmake/dumux/porousmediumflow
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("tracer")
subdirs("tracermin")
subdirs("tracermineralization")
