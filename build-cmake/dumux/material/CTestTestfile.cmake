# CMake generated Testfile for 
# Source directory: /home/rwinter/DUMUX/winter2023a/dumux/material
# Build directory: /home/rwinter/DUMUX/winter2023a/build-cmake/dumux/material
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("components")
subdirs("fluidsystems")
subdirs("solidstates")
subdirs("solidsystems")
