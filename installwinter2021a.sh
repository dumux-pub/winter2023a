# dune-common
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.7
git reset --hard 94d28a4d4462268f9eeef11459cdf5d53cb45c1e
cd ..

# dune-geometry
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.7
git reset --hard b32bf288d85501c472a69d85512500eda75fd41c
cd ..

# dune-grid
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.7
git reset --hard b7741c6599528bc42017e25f70eb6dd3b5780277
cd ..

# dune-localfunctions
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.7
git reset --hard 5e2c4c9239bbdeab7957557e13e47016c7aa50b9
cd ..

# dune-istl
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.7
git reset --hard da96303c0a2d1ee46911cbdd973bf470b3275c11
cd ..

# dumux
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout releases/3.4
git reset --hard 6bd41c132ef4d3496e868c90313b78e47816733a
cd ..


# installwinter2021a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/winter2021a.git

./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
