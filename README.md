# Winter2023a

Welcome to the dumux pub module for Winter2023a
======================================================

This module contains the source code for the examples in Project RADIOMOD


Installation
============

For building from source create a folder and download [installWinter2023a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/winter2023a/-/blob/master/installwinter2023a.sh) from this repository

to that folder and run the script with

```
chmod u+x installwinter2023a.sh
./installwinter2023a.sh
```

Installation with Docker 
========================

Create a new folder in your favourite location and download the container startup script to that folder.
```bash
mkdir Winter2023a
cd Winter2023a
wget https://git.iws.uni-stuttgart.de/dumux-pub/winter2023a/-/raw/master/docker_winter2023a.sh
```

Open the Docker Container by running
```bash
bash docker_winter2023a.sh open
```

Reproducing the results
=======================
After the script has run successfully, you may build program executables e.g.

```
cd winter202a/build-cmake/appl/2p
make test_2p_tpfa

and run them, e.g., with

```
./test_2p_tpfa
```

