// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup RichardsTests
 * \brief Spatial parameters for the RichardsRadioactiveTestProblem.
 */

#ifndef DUMUX_RICHARDS_RADIOACTIVETEST_SPATIAL_PARAMS_HH
#define DUMUX_RICHARDS_RADIOACTIVETEST_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/porousmediumflow/richards/model.hh>
#include <dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh>

namespace Dumux {

/*!
 * \ingroup RichardsTests
 * \brief The spatial parameters for the RichardsNCRadioactiveTestProblem.
 */
template<class GridGeometry, class Scalar>
class RichardsCompTestSpatialParams
: public FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, RichardsCompTestSpatialParams<GridGeometry, Scalar>>
{
    using ThisType = RichardsCompTestSpatialParams<GridGeometry, Scalar>;
    using ParentType = FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, ThisType>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
public:
    using PcKrSwCurve = FluidMatrix::VanGenuchtenDefault<Scalar>;
    using PermeabilityType = Scalar;


    RichardsCompTestSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        porosity_ = getParam<Scalar>("SpatialParams.N", 0.35);
        permeability_ = getParam<Scalar>("SpatialParams.K", 1e-11);

        vGN_ = getParam<Scalar>("SpatialParams.VanGenuchtenN", 1.56);
        vGAlpha_ = getParam<Scalar>("SpatialParams.VanGenuchtenAlpha", 3.6/(1000*9.81));
        swr_ = getParam<Scalar>("SpatialParams.Swr", 0.0);
        snr_ = getParam<Scalar>("SpatialParams.Snr", 0.0);
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *        In this test, we use element-wise distributed permeabilities.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    { return permeability_; }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The porosity
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    { return porosity_; }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The current element
     * \return The porosity
     */
    Scalar porosity(const Element& element) const
    {
        return porosity_;
    }

    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law.
     *
     * In this test, we use element-wise distributed material parameters.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The material parameters object
     */
    template<class ElementSolution>
    auto fluidMatrixInteraction(const Element& element,
                                const SubControlVolume& scv,
                                const ElementSolution& elemSol) const
    {
        typename PcKrSwCurve::BasicParams params(vGAlpha_, vGN_);
        typename PcKrSwCurve::EffToAbsParams effToAbsParams(swr_, snr_);
        return makeFluidMatrixInteraction(PcKrSwCurve(params, effToAbsParams));
    }

    /*!
     * \brief Returns the fluid-matrix interaction law an element
     *
     * \param element The current finite element
     * \return The material parameters object
     */
    auto fluidMatrixInteraction(const Element& element) const
    {
        typename PcKrSwCurve::BasicParams params(vGAlpha_, vGN_);
        typename PcKrSwCurve::EffToAbsParams effToAbsParams(swr_, snr_);
        return makeFluidMatrixInteraction(PcKrSwCurve(params, effToAbsParams));
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \param globalPos The global position
     * \return The wetting phase index
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }

private:
    Scalar porosity_;
    Scalar permeability_;

    Scalar vGN_;
    Scalar vGAlpha_;
    Scalar swr_;
    Scalar snr_;

    static constexpr Scalar eps_ = 1.5e-7;
};

} // end namespace Dumux

#endif
