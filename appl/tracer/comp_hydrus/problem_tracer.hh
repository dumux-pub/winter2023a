// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup TracerTests
 * \brief  A Richards problem with possible multiple radioactive tracers
 */
#ifndef DUMUX_LFRITRACER_TEST_PROBLEM_HH
#define DUMUX_LFTRIRACER_TEST_PROBLEM_HH

#include <dune/grid/uggrid.hh>

#include <dumux/common/boundarytypes.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/tracer/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/fluidsystems/base.hh>
#include "dumux/material/fluidsystems/fluidsystem_tracer_i129.hh"
#include <dumux/material/fluidmatrixinteractions/dispersiontensors/fulltensor.hh>
#include <dumux/material/fluidmatrixinteractions/dispersiontensors/scheidegger.hh>

#include "spatialparams_tracer.hh"

namespace Dumux {
/*!
 * \ingroup TracerTests
 * \brief A tracer problem with possible multiple radioactive tracers
 */
template <class TypeTag>
class CompRiTracerTestProblem;

namespace Properties {
//Create new type tags
namespace TTag {
struct CompRiTracerTest { using InheritsFrom = std::tuple<Tracer>; };
struct CompRiTracerTestCCTpfa { using InheritsFrom = std::tuple<CompRiTracerTest, CCTpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::CompRiTracerTest> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::CompRiTracerTest> { using type = CompRiTracerTestProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::CompRiTracerTest>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = CompRiTracerTestSpatialParams<GridGeometry, Scalar>;
};

template<class TypeTag>
struct CompositionalDispersionModel<TypeTag, TTag::CompRiTracerTest>
{ using type = ScheideggersDispersionTensor<TypeTag>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::CompRiTracerTest> { using type = FluidSystems::TracerFluidSystem<TypeTag>; };

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::CompRiTracerTest> { static constexpr bool value = true; };
template<class TypeTag>
struct SolutionDependentMolecularDiffusion<TypeTag, TTag::CompRiTracerTestCCTpfa> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableCompositionalDispersion<TypeTag, TTag::CompRiTracerTest> { static constexpr bool value = true; };
} // end namespace Properties

/*!
 * \ingroup TracerTests
 *
 * \brief Definition of a problem, for the tracer problem:
 *
 * This problem uses the \ref TracerModel model.
 */
template <class TypeTag>
class CompRiTracerTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using TracerSolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;

    //! property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();
    static constexpr bool useDispersion = getPropValue<TypeTag, Properties::EnableCompositionalDispersion>();
    static constexpr int numComponents = FluidSystem::numComponents;
    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld,
        conti0EqIdx = Indices::conti0EqIdx,
    };

public:
    CompRiTracerTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        if(useDispersion)
            std::cout<<"problem uses dispersion" << '\n';
        /// stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout<<"problem uses mole fractions" << '\n';
        else
            std::cout<<"problem uses mass fractions" << '\n';

        initialValue_ = getParam<Scalar>("Problem.InitialTracerValue", -1e3); //Bq/kg
    }

    void setTimeStepSize( Scalar timeStepSize )
    {
        timeStepSize_ = timeStepSize;
    }

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        if (onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos))
            values.setAllDirichlet();
        else
            values.setAllNeumann();

        return values;
    }
    // \}

    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment.
     *
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        return values;
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
    * \param element The finite element
     */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolumeFace &scvf) const
    {
        PrimaryVariables values(0.0);
        const auto& globalPos = scvf.ipGlobal();

        if (onUpperBoundary_(globalPos))
        {
            values[0] = 1e-3;
        }

        return values;
    }

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables initialValues(0.0);
        return initialValues;
    }

    /*!
     * \brief Returns the time for a variable neumann boundary condition.
     *
     */
    void setTime(Scalar time)
    {
        time_ = time;
    }


private:
    static constexpr Scalar eps_ = 1e-6;
    Scalar time_;
    Scalar timeStepSize_;
    Scalar initialValue_;

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_;
    }
};

} //end namespace Dumux

#endif
