// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \ingroup RichardsTests
 * \brief The properties for the flow problem of a radioactive richards test.
 */
#ifndef DUMUX_LFRICHARDSFLOW_TEST_PROBLEM_HH
#define DUMUX_LFRICHARDSFLOW_TEST_PROBLEM_HH

#include <dune/grid/uggrid.hh>
#include <iostream>
#include <cmath>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/porousmediumflow/richards/model.hh>

#include "spatialparams_ri.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class RichardsFlowTestProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct RichardsFlow { using InheritsFrom = std::tuple<Richards>; };
struct RichardsFlowTpfa { using InheritsFrom = std::tuple<RichardsFlow, CCTpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::RichardsFlow> { using type = Dune::UGGrid<2>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::RichardsFlow> { using type = RichardsFlowTestProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::RichardsFlow>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = RichardsFlowTestSpatialParams<GridGeometry, Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::RichardsFlow> { static constexpr bool value = true; };
}// end namespace Properties

/*!
 * \ingroup RichardsTests
 * \brief The properties for the flow problem of a radioactive 2p test.
 */
template<class TypeTag>
class RichardsFlowTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using PcKrSwCurve = typename ParentType::SpatialParams::PcKrSwCurve;
    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld,
        pressureIdx = Indices::pressureIdx,
        // equation indices
        conti0EqIdx = Indices::conti0EqIdx + FluidSystem::H2OIdx,
    };

public:
    RichardsFlowTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        injectionMass_ = getParam<Scalar>("Problem.InjectionMass", -0.0);
        hasAnnualRainCycle_ = getParam<bool>("Problem.HasAnnualRainCycle", false);
        hasRainEvents_ = getParam<bool>("Problem.HasRainEvents", false);
        hasSurfaceRunoff_ = getParam<bool>("Problem.HasSurfaceRunoff", false);
        hasEvaporation_ = getParam<bool>("Problem.HasEvaporation", false);
        evaporationRate_ = getParam<Scalar>("Problem.EvaporationRate", 0.0);

        if(hasRainEvents_)
        {
            daysBtwEvents_ = getParam<Scalar>("Problem.DaysBtwEvents", 0.0);
            tEnd_ = getParam<Scalar>("TimeLoop.TEnd");
            Scalar timeBtwEvents_ = daysBtwEvents_*24*3600;
            Scalar eventTime_ = 0.0;
            uint64_t i = 0;
            while (i < tEnd_)
            {
                if(i==0)
                {
                    rainStartPoints_.push_back(i);
                    i += 24*3600;
                    rainEndPoints_.push_back(i);
                    eventTime_ = i;
                }
                else if (i >= eventTime_ + timeBtwEvents_)
                {
                    rainStartPoints_.push_back(i);
                    i += 24*3600;
                    rainEndPoints_.push_back(i);
                    eventTime_ = i;
                }
                else
                    i += 1;
            }
        }
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        if (onRightBoundary_(globalPos))
            values.setAllDirichlet();
        else
            values.setAllNeumann();

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann
     *        boundary segment in dependency on the current solution.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub-control volume face
     *
     * This method is used for cases, when the Neumann condition depends on the
     * solution and requires some quantities that are specific to the fully-implicit method.
     * The \a values store the mass flux of each phase normal to the boundary.
     * Negative values indicate an inflow.
     */
    template<class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        const auto& globalPos = scvf.ipGlobal();
        if (onUpperBoundary_(globalPos))
        {
            if(hasAnnualRainCycle_)
                values[conti0EqIdx] = (sin(2*M_PI*time_/31536000.0)+1)*injectionMass_;
            else if(hasRainEvents_)
            {
                if (isRainEvent(time_, rainStartPoints_, rainEndPoints_))
                {
                    if(hasSurfaceRunoff_)
                    {
                        //SCS - Method after Maniak, 2016 Hydrologie und Wasserwirtschaft
                        Scalar CN = getParam<Scalar>("Problem.CN", 1e-10);
                        Scalar rainMass = injectionMass_*(1 + daysBtwEvents_);
                        //Maniak mentions the usual measurements is precipitation per day, so we calculate
                        //kg/m^2 s into mm/d: mm/d = kg/m^2 s * s/d * 1000mm/m / 1000kg/m^3
                        Scalar rainMass_mm = abs(rainMass*3600*24);
                        //discharge coefficient
                        Scalar S_max = 25400/CN - 254;
                        //Maniak: 0.2 * S_max if I_a < N, not always the case, so set it 0 for now
                        Scalar I_a = 0.0;
                        Scalar surfaceRunoff = std::pow((rainMass_mm - I_a), 2)/(rainMass_mm - I_a + S_max);
                        values[conti0EqIdx] =  -(abs(rainMass) - abs(surfaceRunoff/(3600*24)));
                    }
                    else
                        values[conti0EqIdx] = injectionMass_*(1 + daysBtwEvents_);
                }
                else
                {
                    if(hasEvaporation_)
                        values[conti0EqIdx] = evaporationRate_;
                    else
                        values[conti0EqIdx] = 0.0;
                }
            }
            else
                values[conti0EqIdx] = injectionMass_;
        }
        else if (onLeftBoundary_(globalPos) && isAquifer_(globalPos))
            values[conti0EqIdx] = -1.27e-2 * this->spatialParams().getPorosity(element); //400m/y in kg/s*m^2

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
    * \param element The finite element
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolumeFace &scvf) const
    {
        PrimaryVariables values(0.0);
        values = initial(element);
        return values;
    }

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param element The finite element
     */
    PrimaryVariables initial(const Element& element) const
    {
        PrimaryVariables values(0.0);
        const auto& globalPos =  element.geometry().center();
        if (isAquifer_(globalPos))
        {
            const Scalar sw = 1.0;
            const Scalar pc = this->spatialParams().fluidMatrixInteraction(element).pc(sw);
            values[pressureIdx] = nonwettingReferencePressure() - pc;
        }
        else
        {
            const Scalar sw = this->spatialParams().swr(element);
            const Scalar pc = this->spatialParams().fluidMatrixInteraction(element).pc(sw);
            values[pressureIdx] = nonwettingReferencePressure() - pc;
        }

        return values;
    }

    /*!
     * \brief Checks in the rain vectors if there is a rain event at the current time
     */
    bool isRainEvent(const Scalar& time, const std::vector<Scalar>& rainStartVector, const std::vector<Scalar>& rainEndVector) const
    {
        for(uint i=0; i<rainStartVector.size(); ++i)
        {
            if(time >= rainStartVector[i] && time < rainEndVector[i])
                return true;
        }
        return false;
    }

    /*!
     * \brief Returns the starting time points for the rain events
     */
    std::vector<Scalar> getRainStartPoints()
    { return rainStartPoints_; }

    /*!
     * \brief Returns the ending time points for the rain events
     */
    std::vector<Scalar> getRainEndPoints()
    { return rainEndPoints_; }

    /*!
     * \brief Returns the reference pressure [Pa] of the non-wetting
     *        fluid phase within a finite volume
     *
     * This problem assumes a constant reference pressure of 1 bar.
     */
    Scalar nonwettingReferencePressure() const
    { return 1.0e5; };

    /*!
     * \brief Returns the timestepsize.
     *
     */
    void setTimeStepSize( Scalar timeStepSize )
    { timeStepSize_ = timeStepSize; }

    /*!
     * \brief Returns the time for a variable neumann boundary condition.
     *
     */
    void setTime(Scalar time)
    { time_ = time; };


private:
    bool isAquifer_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] < 1 + eps_;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] < this->gridGeometry().bBoxMin()[dim-1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] > this->gridGeometry().bBoxMax()[dim-1] - eps_;
    }

    static constexpr Scalar eps_ = 1e-6;

    Scalar time_, tEnd_, timeStepSize_;
    Scalar injectionMass_;

    bool hasRainEvents_, hasRainLF_, hasAnnualRainCycle_, hasEvaporation_, hasSurfaceRunoff_;
    Scalar daysBtwEvents_, evaporationRate_;
    std::vector<Scalar> rainStartPoints_, rainEndPoints_;
};

} // end namespace Dumux

#endif
