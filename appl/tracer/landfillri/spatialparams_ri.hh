// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \ingroup RichardsTests
 * \brief The spatial params for the flow problem of a radioactive 2p test.
 */

#ifndef DUMUX_LFRICHARDSFLOW_TEST_SPATIAL_PARAMS_HH
#define DUMUX_LFRICHARDSFLOW_TEST_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/porousmediumflow/richards/model.hh>
#include <dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh>

namespace Dumux {

/*!
 * \ingroup RichardsTests
 * \brief The spatial params for the flow problem of a radioactive 2p test.
 */
template<class GridGeometry, class Scalar>
class RichardsFlowTestSpatialParams
: public FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, RichardsFlowTestSpatialParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ThisType = RichardsFlowTestSpatialParams<GridGeometry, Scalar>;
    using ParentType = FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, ThisType>;

    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    using PcKrSwCurve = FluidMatrix::VanGenuchtenDefault<Scalar>;
    using PermeabilityType = Scalar;

    RichardsFlowTestSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    , n_(gridGeometry->gridView().size(0), 0.0)
    , K_(gridGeometry->gridView().size(0), 0.0)
    , swr_(gridGeometry->gridView().size(0), 0.0)
    , snr_(gridGeometry->gridView().size(0), 0.0)
    , vgAlpha_(gridGeometry->gridView().size(0), 0.0)
    , vgN_(gridGeometry->gridView().size(0), 0.0)
    , vgL_(gridGeometry->gridView().size(0), 0.0)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto& globalPos = element.geometry().center();
            const auto eIdx = this->gridGeometry().elementMapper().index(element);
            if (globalPos[dim-1] > 1+5)
            {
                n_[eIdx] = 0.46;
                K_[eIdx] = getParam<Scalar>("SpatialParams.K_LF", 1.913e-11);
                swr_[eIdx] = 0.13/n_[eIdx];
                snr_[eIdx] = 0.001;
                vgAlpha_[eIdx] = 28.4/(1000*9.81);
                vgN_[eIdx] = 2.556;
                vgL_[eIdx] = -0.637;
            }
            else if (globalPos[dim-1] > 1)
            {
                n_[eIdx] = 0.43;
                K_[eIdx] = 2602/(7.5e6 * 3600 * 24* 365);
                swr_[eIdx] = 0.045/n_[eIdx];
                snr_[eIdx] = 0.001;
                vgAlpha_[eIdx] = 14.5/(1000*9.81);
                vgN_[eIdx] = 2.68;
                vgL_[eIdx] = 0.5;
            }
            else
            {
                n_[eIdx] = 0.43;
                K_[eIdx] = 2602/(7.5e6 * 3600 * 24* 365);
                swr_[eIdx] = 0.13/n_[eIdx];
                snr_[eIdx] = 0.001;
                vgAlpha_[eIdx] = 28.4/(1000*9.81);
                vgN_[eIdx] = 2.68;
                vgL_[eIdx] = 0.5;
            }
        }
    }

    //! get the permeability field for output
    const std::vector<Scalar>& getPermField() const
    {return K_; }

    //! get the porosity for the problem
    Scalar getPorosity(const Element& element) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        return n_[eIdx];
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *        In this test, we use element-wise distributed permeability.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    { return K_[scv.dofIndex()]; }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The porosity
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    { return n_[scv.dofIndex()]; }

    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law.
     *
     * In this test, we use element-wise distributed material parameters.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The material parameters object
     */
    template<class ElementSolution>
    auto fluidMatrixInteraction(const Element& element,
                                const SubControlVolume& scv,
                                const ElementSolution& elemSol) const
    {
        typename PcKrSwCurve::BasicParams params(vgAlpha_[scv.dofIndex()], vgN_[scv.dofIndex()], vgL_[scv.dofIndex()]);
        typename PcKrSwCurve::EffToAbsParams effToAbsParams(swr_[scv.dofIndex()], snr_[scv.dofIndex()]);
        return makeFluidMatrixInteraction(PcKrSwCurve(params, effToAbsParams));
    }

    /*!
     * \brief Returns the fluid-matrix interaction law an element
     *
     * \param element The current finite element
     */
    auto fluidMatrixInteraction(const Element& element) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);

        typename PcKrSwCurve::BasicParams params(vgAlpha_[eIdx], vgN_[eIdx], vgL_[eIdx]);
        typename PcKrSwCurve::EffToAbsParams effToAbsParams(swr_[eIdx], snr_[eIdx]);
        return makeFluidMatrixInteraction(PcKrSwCurve(params, effToAbsParams));
    }

    /*!
     * \brief Returns the residual water saturation \f$[-]\f$
     *
     * \param element The current element
     */
    Scalar swr(const Element& element) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        return swr_[eIdx];
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \param globalPos The global position
     * \return The wetting phase index
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }

private:
    std::vector<Scalar> n_, K_, swr_, snr_, vgAlpha_, vgN_, vgL_;
    static constexpr Scalar eps_ = 1.5e-7;
};

} // end namespace Dumux

#endif
