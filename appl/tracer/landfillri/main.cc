// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup TracerTests
 * \brief Test for the richards tracer CC model
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <dumux/io/container.hh>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>

#include "problem_ri.hh"
#include "problem_tracer.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/porousmediumflow/richards/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/columngridmanager.hh>

template <class MoleFractionVector, class SaturationVector>
void equilibrateTracer(MoleFractionVector& moleFracVec,
                       const SaturationVector& oldSatVec,
                       const SaturationVector& newSatVec)
{
    for (auto i = 0u; i < moleFracVec.size(); ++i)
    {
        auto newSat = newSatVec[i];
        if (!Dune::FloatCmp::eq(newSat, 0.0))
            moleFracVec[i] *= oldSatVec[i]/newSat;
    }
}

int main(int argc, char** argv)
{
    using namespace Dumux;

    //! define the type tags for this problem
    using FlowTypeTag = Properties::TTag::RichardsFlowTpfa;
    using TracerTypeTag = Properties::TTag::LFRiTracerTestTpfa;
    //! initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    //! print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    ColumnGridManager<GetPropType<FlowTypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    // get some time loop parameters
    using Scalar =  GetPropType<FlowTypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = getParam<Scalar>("Restart.Time", 0);

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    ////////////////////////////////////////////////////////////
    // set flow Problem
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<FlowTypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);

    // the problem (initial and boundary conditions)
    using FlowTypeProblem = GetPropType<FlowTypeTag, Properties::Problem>;
    auto flowTypeProblem = std::make_shared<FlowTypeProblem>(gridGeometry);

    // the solution vector
    using FlowTypeSolutionVector = GetPropType<FlowTypeTag, Properties::SolutionVector>;
    FlowTypeSolutionVector p(gridGeometry->numDofs());
    flowTypeProblem->applyInitialSolution(p);
    auto pOld = p;

    // the grid variables
    using FlowTypeGridVariables = GetPropType<FlowTypeTag, Properties::GridVariables>;
    auto flowTypeGridVariables = std::make_shared<FlowTypeGridVariables>(flowTypeProblem, gridGeometry);
    flowTypeGridVariables->init(p);

    // intialize the vtk output module
    using FlowTypeIOFields = GetPropType<FlowTypeTag, Properties::IOFields>;

    // use non-conforming output for the test with interface solver
    const auto ncOutput = getParam<bool>("Problem.UseNonConformingOutput", false);
    VtkOutputModule<FlowTypeGridVariables, FlowTypeSolutionVector> flowTypeVtkWriter(*flowTypeGridVariables, p, flowTypeProblem->name()+"_ri", "",
                                                                        (ncOutput ? Dune::VTK::nonconforming : Dune::VTK::conforming));

    FlowTypeIOFields::initOutputModule(flowTypeVtkWriter); //!< Add model specific output fields
    flowTypeVtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using FlowTypeAssembler = FVAssembler<FlowTypeTag, DiffMethod::numeric>;
    auto flowTypeAssembler = std::make_shared<FlowTypeAssembler>(flowTypeProblem, gridGeometry, flowTypeGridVariables, timeLoop, pOld);

    // the linear solver
    using FlowTypeLinearSolver = Dumux::ILUBiCGSTABIstlSolver<LinearSolverTraits<GridGeometry>, LinearAlgebraTraitsFromAssembler<FlowTypeAssembler>>;
    auto flowTypeLinearSolver = std::make_shared<FlowTypeLinearSolver>(leafGridView, gridGeometry->dofMapper());

    // the non-linear solver
    using NewtonSolver = Dumux::RichardsNewtonSolver<FlowTypeAssembler, FlowTypeLinearSolver>;
    NewtonSolver nonLinearSolver(flowTypeAssembler, flowTypeLinearSolver);

    ////////////////////////////////////////////////////////////
    // set tracer Problem
    ////////////////////////////////////////////////////////////

    //! the problem (initial and boundary conditions)
    using TracerProblem = GetPropType<TracerTypeTag, Properties::Problem>;
    auto tracerProblem = std::make_shared<TracerProblem>(gridGeometry);

    //! the solution vector
    using TracerSolutionVector = GetPropType<TracerTypeTag, Properties::SolutionVector>;
    TracerSolutionVector x(leafGridView.size(0));
    tracerProblem->applyInitialSolution(x);
    auto xOld = x;

    //! initialize the flux, density, saturation and velocity vectors
    std::vector<Scalar> volumeFlux_(gridGeometry->numScvf(), 0.0);
    std::vector<Scalar> density_(gridGeometry->numScv(), 0.0);
    std::vector<Scalar> saturation_(gridGeometry->numScv(), 0.0);
    std::vector<Scalar> oldSaturation_(gridGeometry->numScv(), 0.0);
    using GridView = typename GridGeometry::GridView;
    using GlobalPosition = typename Dune::FieldVector<Scalar, GridView::dimensionworld>;
    std::vector<GlobalPosition> velocity_(gridGeometry->numScvf(), GlobalPosition(0.0));

    //! the grid variables
    using TracerGridVariables = GetPropType<TracerTypeTag, Properties::GridVariables>;
    auto tracerGridVariables = std::make_shared<TracerGridVariables>(tracerProblem, gridGeometry);
    tracerGridVariables->init(x);

    //! the linear system
    using JacobianMatrix = GetPropType<TracerTypeTag, Properties::JacobianMatrix>;
    auto A = std::make_shared<JacobianMatrix>();
    auto r = std::make_shared<TracerSolutionVector>();

    //! the assembler with time loop for instationary problem
    using TracerAssembler = FVAssembler<TracerTypeTag, DiffMethod::analytic, /*implicit=*/true>;
    auto tracerAssembler = std::make_shared<TracerAssembler>(tracerProblem, gridGeometry, tracerGridVariables, timeLoop, xOld);
    tracerAssembler->setLinearSystem(A, r);

    // the linear solver
    using TracerLinearSolver = Dumux::ILUBiCGSTABIstlSolver<LinearSolverTraits<GridGeometry>, LinearAlgebraTraitsFromAssembler<TracerAssembler>>;
    auto tracerLinearSolver = std::make_shared<TracerLinearSolver>(leafGridView, gridGeometry->dofMapper());

    // set the flux, density and saturation from the flow problem
    tracerProblem->spatialParams().setVolumeFlux(volumeFlux_);
    tracerProblem->spatialParams().setDensity(density_);
    tracerProblem->spatialParams().setSaturation(saturation_);

    //! initialize the vtk output module
    VtkOutputModule<TracerGridVariables, TracerSolutionVector> vtkWriter(*tracerGridVariables, x, tracerProblem->name());
    using TracerIOFields = GetPropType<TracerTypeTag, Properties::IOFields>;
    TracerIOFields::initOutputModule(vtkWriter); //!< Add model specific output fields
    using VelocityOutput = GetPropType<TracerTypeTag, Properties::VelocityOutput>;
    vtkWriter.addVelocityOutput(std::make_shared<VelocityOutput>(*tracerGridVariables));
    tracerProblem->addFieldsToWriter(vtkWriter); //!< Add some more problem dependent fields
    vtkWriter.write(0.0);

    const auto hasRainEvents_ = getParam<bool>("Problem.HasRainEvents", false);
    if(hasRainEvents_)
    {
        std::vector<Scalar> rainStartPoints_ = flowTypeProblem->getRainStartPoints();
        std::vector<Scalar> rainEndPoints_ = flowTypeProblem->getRainEndPoints();
        std::vector<Scalar> rainCheckPoints_((Scalar)rainStartPoints_.size() + (Scalar)rainEndPoints_.size());
        std::merge(rainStartPoints_.begin(), rainStartPoints_.end(), rainEndPoints_.begin(), rainEndPoints_.end(), rainCheckPoints_.begin());
        rainCheckPoints_.erase(unique(rainCheckPoints_.begin(), rainCheckPoints_.end()), rainCheckPoints_.end());
        timeLoop->setCheckPoint(rainCheckPoints_);
    }
    else
    {
        //! set some check points for the time loop
        timeLoop->setPeriodicCheckPoint(tEnd/500.0);
    }

    //initialize the saturation from the flow problem
    for (const auto& element : elements(leafGridView))
    {
        auto eIdx = gridGeometry->elementMapper().index(element);
        const auto fluidMatrixInteraction = flowTypeProblem->spatialParams().fluidMatrixInteraction(element);
        using std::max;
        const Scalar pc = max(fluidMatrixInteraction.endPointPc(),
                              flowTypeProblem->nonwettingReferencePressure() - flowTypeProblem->initial(element)[0]);
        const Scalar sw = fluidMatrixInteraction.sw(pc);

        oldSaturation_[eIdx] = sw;
        saturation_[eIdx] = sw;
    }

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem
    ////////////////////////////////////////////////////////////

    // time loop
    timeLoop->start(); do
    {
        // set the time and timeStepSize for the problem
        flowTypeProblem->setTime(timeLoop->time());
        flowTypeProblem->setTimeStepSize(timeLoop->timeStepSize());
        // solve the non-linear system with time step control
        nonLinearSolver.solve(p, *timeLoop);

        // make the new solution the old solution
        pOld = p;
        oldSaturation_ = saturation_;
        flowTypeGridVariables->advanceTimeStep();

        // loop over elements to compute fluxes, saturations, densities for tracer
        using FluxVariables = GetPropType<FlowTypeTag, Properties::FluxVariables>;
        auto upwindTerm = [](const auto& volVars) { return volVars.mobility(0); };
        for (const auto& element : elements(leafGridView))
        {
            auto fvGeometry = localView(*gridGeometry);
            fvGeometry.bind(element);

            auto elemVolVars = localView(flowTypeGridVariables->curGridVolVars());
            elemVolVars.bind(element, fvGeometry, p);

            auto elemFluxVars = localView(flowTypeGridVariables->gridFluxVarsCache());
            elemFluxVars.bind(element, fvGeometry, elemVolVars);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                const auto idx = scvf.index();
                if (!scvf.boundary())
                {
                    FluxVariables fluxVars;
                    fluxVars.init(*flowTypeProblem, element, fvGeometry, elemVolVars, scvf, elemFluxVars);
                    volumeFlux_[idx] = fluxVars.advectiveFlux(0, upwindTerm);
                    velocity_[idx] = volumeFlux_[idx] * scvf.unitOuterNormal()
                                        /(scvf.area() * elemVolVars[fvGeometry.scv(scvf.insideScvIdx())].extrusionFactor());
                }
                else
                {
                    const auto bcTypes = flowTypeProblem->boundaryTypes(element, scvf);

                    if (bcTypes.hasOnlyDirichlet())
                    {
                        FluxVariables fluxVars;
                        fluxVars.init(*flowTypeProblem, element, fvGeometry, elemVolVars, scvf, elemFluxVars);
                        volumeFlux_[idx] = fluxVars.advectiveFlux(0, upwindTerm);
                        velocity_[idx] = volumeFlux_[idx] * scvf.unitOuterNormal()
                                        /(scvf.area() * elemVolVars[fvGeometry.scv(scvf.insideScvIdx())].extrusionFactor());
                    }
                    else if (bcTypes.hasOnlyNeumann())
                    {
                        volumeFlux_[idx] = flowTypeProblem->neumann(element, fvGeometry, elemVolVars, elemFluxVars, scvf)
                                        /elemVolVars[scvf.insideScvIdx()].fluidState().density(0);
                        velocity_[idx] = volumeFlux_[idx] * scvf.unitOuterNormal()
                                        /(scvf.area() * elemVolVars[fvGeometry.scv(scvf.insideScvIdx())].extrusionFactor());
                    }
                }
            }

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                const auto idx = scv.dofIndex();

                density_[idx] = volVars.density(0);
                saturation_[idx] = volVars.saturation(0);
            }
        }


        ////////////////////////////////////////////////////////////
        // solve tracer problem on the same grid
        ////////////////////////////////////////////////////////////

        // set the current time for the problem
        tracerProblem->setTime(timeLoop->time());
        tracerProblem->setTimeStepSize(timeLoop->timeStepSize());

        // set the flux from the flow problem
        tracerProblem->spatialParams().setDensity(density_);
        tracerProblem->spatialParams().setSaturation(saturation_);
        tracerProblem->spatialParams().setVolumeFlux(volumeFlux_);
        tracerProblem->spatialParams().setVelocity(velocity_);
        equilibrateTracer(xOld, oldSaturation_, saturation_);
        x = xOld;

        Dune::Timer tracerAssembleTimer;
        tracerAssembler->assembleJacobianAndResidual(x);
        tracerAssembleTimer.stop();

        // solve the linear system A(xOld-xNew) = r
        Dune::Timer solveTimer;
        TracerSolutionVector xDelta(x);
        tracerLinearSolver->solve(*A, xDelta, *r);
        solveTimer.stop();

        // update solution and grid variables
        Dune::Timer updateTimer;
        updateTimer.reset();
        x -= xDelta;
        tracerGridVariables->update(x);
        updateTimer.stop();
        // statistics
        Dune::Timer assembleTimer;
        const auto elapsedTot = assembleTimer.elapsed() + solveTimer.elapsed() + updateTimer.elapsed();
        std::cout << "Assemble/solve/update time: "
                  <<  assembleTimer.elapsed() << "(" << 100*assembleTimer.elapsed()/elapsedTot << "%)/"
                  <<  solveTimer.elapsed() << "(" << 100*solveTimer.elapsed()/elapsedTot << "%)/"
                  <<  updateTimer.elapsed() << "(" << 100*updateTimer.elapsed()/elapsedTot << "%)"
                  <<  std::endl;

        // make the new solution the old solution
        xOld = x;
        tracerGridVariables->advanceTimeStep();

        // update the output fields before write
        tracerProblem->updateVtkFields(x);


        // advance the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        if (timeLoop->isCheckPoint() || timeLoop->finished())
        {
            flowTypeVtkWriter.write(timeLoop->time());
            vtkWriter.write(timeLoop->time());
        }

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the Newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));
    } while (!timeLoop->finished());
    timeLoop->finalize(leafGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    //! print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
