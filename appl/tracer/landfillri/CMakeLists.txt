add_input_file_links()

dumux_add_test(NAME test_lfri_tpfa
              LABELS porousmediumflow richards tracer
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK)
