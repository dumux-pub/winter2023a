// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup TracerTests
 * \brief Definition of the spatial parameters for a problem with possible multiple radioactive tracers
 */
#ifndef DUMUX_LFRITRACER_TEST_SPATIAL_PARAMS_HH
#define DUMUX_LFRITRACER_TEST_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/properties.hh>
#include <dumux/porousmediumflow/fvspatialparams1p.hh>

namespace Dumux {

/*!
 * \ingroup TracerTests
 * \brief Definition of the spatial parameters for a problem with possible multiple radioactive tracers
 */
template<class GridGeometry, class Scalar>
class LFRiTracerTestSpatialParams
: public FVPorousMediumFlowSpatialParamsOneP<GridGeometry, Scalar, LFRiTracerTestSpatialParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;
    using ParentType = FVPorousMediumFlowSpatialParamsOneP<GridGeometry, Scalar, LFRiTracerTestSpatialParams<GridGeometry, Scalar>>;
    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };
    using GlobalPosition = typename Dune::FieldVector<Scalar, dimWorld>;
    using DimWorldMatrix = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;

public:

    LFRiTracerTestSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        alphaL_ = getParam<Scalar>("Problem.AlphaL");
        alphaT_ = getParam<Scalar>("Problem.AlphaT");
        deltaH_ = getParam<Scalar>("Grid.DeltaH", 0.0);

        dispersionTensorCoefficients_ = getParam<std::vector<Scalar>>("Problem.DispersionTensor");
        for (int i = 0; i < dimWorld; i++)
                dispersionTensor_[i][i] = dispersionTensorCoefficients_[0];
    }

    /*!
     * \brief Defines the porosity \f$\mathrm{[-]}\f$.
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        if (isLandfill_(globalPos))
            return 0.43;
        else
            return 0.46;
    }

    //! Fluid properties that are spatial parameters in the tracer model
    //! They can possible vary with space but are usually constants

    //! Fluid density
    Scalar fluidDensity(const Element &element,
                        const SubControlVolume& scv) const
    { return 1000; }

    void setDensity(const std::vector<Scalar>& d)
    { density_ = d; }

    //! fluid molar mass
    Scalar fluidMolarMass(const Element &element,
                          const SubControlVolume& scv) const
    { return 0.018; }

    Scalar fluidMolarMass(const GlobalPosition &globalPos) const
    { return 0.018; }

    //! Velocity field
    template<class ElementVolumeVariables>
    Scalar volumeFlux(const Element &element,
                      const FVElementGeometry& fvGeometry,
                      const ElementVolumeVariables& elemVolVars,
                      const SubControlVolumeFace& scvf) const
    { return volumeFlux_[scvf.index()]; }

    void setVolumeFlux(const std::vector<Scalar>& f)
    { volumeFlux_ = f; }

    GlobalPosition velocity(const SubControlVolumeFace& scvf) const
    { return velocity_[scvf.index()]; }

    void setVelocity(const std::vector<GlobalPosition>& f)
    { velocity_ = f; }

    //! saturation from Problem
    Scalar saturation(const Element &element,
                      const SubControlVolume& scv) const
    { return saturation_[scv.dofIndex()]; }

    void setSaturation(const std::vector<Scalar>& s)
    { saturation_ = s; }

    /*!
     * \brief Defines the dispersion tensor \f$\mathrm{[-]}\f$.
     *
     * \param globalPos The global position
     */
    std::array<Scalar, 2> dispersionAlphas(const GlobalPosition& globalPos, int phaseIdx = 0, int compIdx = 0) const
    { return { alphaL_, alphaT_ }; }

    /*!
     * \brief Defines the dispersion tensor \f$\mathrm{[-]}\f$.
     *
     * \param globalPos The global position
     */
    const DimWorldMatrix &dispersionTensor(const GlobalPosition& globalPos, int phaseIdx = 0, int compIdx = 0) const
    { return dispersionTensor_; }

private:
    std::vector<Scalar> volumeFlux_, density_, saturation_, dispersionTensorCoefficients_;
    std::vector<GlobalPosition> velocity_;
    Scalar alphaL_, alphaT_;
    DimWorldMatrix dispersionTensor_;
    Scalar deltaH_;
    static constexpr Scalar eps_ = 1.5e-7;

    bool isLandfill_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] > 15 - globalPos[0]*deltaH_ - eps_;
    }
};

} // end namespace Dumux

#endif
