// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup TracerTests
 * \brief  A Richards problem with possible multiple radioactive tracers
 */
#ifndef DUMUX_LFRITRACER_TEST_PROBLEM_HH
#define DUMUX_LFTRIRACER_TEST_PROBLEM_HH

#include <dune/grid/uggrid.hh>
#include <dumux/common/boundarytypes.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/tracer/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/fluidsystems/base.hh>
#include "dumux/material/fluidsystems/fluidsystem_tracer_i129.hh"
#include <dumux/material/fluidmatrixinteractions/dispersiontensors/fulltensor.hh>
#include <dumux/material/fluidmatrixinteractions/dispersiontensors/scheidegger.hh>

#include "spatialparams_tracer.hh"

namespace Dumux {
/*!
 * \ingroup TracerTests
 * \brief A tracer problem with possible multiple radioactive tracers
 */
template <class TypeTag>
class LFRiTracerTestProblem;

namespace Properties {
//Create new type tags
namespace TTag {
struct LFRiTracerTest { using InheritsFrom = std::tuple<Tracer>; };
struct LFRiTracerTestTpfa { using InheritsFrom = std::tuple<LFRiTracerTest, CCTpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::LFRiTracerTest> { using type = Dune::UGGrid<3>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::LFRiTracerTest> { using type = LFRiTracerTestProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::LFRiTracerTest>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = LFRiTracerTestSpatialParams<GridGeometry, Scalar>;
};

template<class TypeTag>
struct CompositionalDispersionModel<TypeTag, TTag::LFRiTracerTest>
{ using type = ScheideggersDispersionTensor<TypeTag>; };
//{ using type = FullDispersionTensor<TypeTag>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::LFRiTracerTest> { using type = FluidSystems::TracerFluidSystem<TypeTag>; };

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::LFRiTracerTest> { static constexpr bool value = true; };
template<class TypeTag>
struct SolutionDependentMolecularDiffusion<TypeTag, TTag::LFRiTracerTestTpfa> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableCompositionalDispersion<TypeTag, TTag::LFRiTracerTest> { static constexpr bool value = true; };

} // end namespace Properties

/*!
 * \ingroup TracerTests
 *
 * \brief Definition of a problem, for the tracer problem:
 *
 * This problem uses the \ref TracerModel model.
 */
template <class TypeTag>
class LFRiTracerTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using TracerSolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;

    //! property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();
    static constexpr bool useDispersion = getPropValue<TypeTag, Properties::EnableCompositionalDispersion>();
    static constexpr int numComponents = FluidSystem::numComponents;
    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld,
        conti0EqIdx = Indices::conti0EqIdx,
    };

public:
    LFRiTracerTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        if(useDispersion)
            std::cout<<"problem uses dispersion" << '\n';
        /// stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout<<"problem uses mole fractions" << '\n';
        else
            std::cout<<"problem uses mass fractions" << '\n';

        deltaH_ = getParam<Scalar>("Grid.DeltaH", 0.0);

        initialValue_ = getParam<Scalar>("Problem.InitialTracerValue", 1e0); // in Bq/g
        useKdModel_ = getParam<bool>("Problem.UseKdModel", false);
        if(useKdModel_)
            std::cout<<"problem uses Kd model" << '\n';
        solidBulkDensity_ = getParam<Scalar>("Problem.SolidBulkDensity", 1500);
    }

    void setTimeStepSize( Scalar timeStepSize )
    {
        timeStepSize_ = timeStepSize;
    }

    /*!
     * \brief Evaluates the source term at a given position.
     * The radioactive decay is given as source term
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub-control volume facevolVars.massFraction(
     */
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);

        const auto& volVars = elemVolVars[scv];
        std::array<Scalar, numComponents> qRadDecay = {0.0};

        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
        {
            if(!FluidSystem::isRadioactive(compIdx))
                continue;

            qRadDecay[compIdx] = useMoles ? - volVars.moleFraction(0, compIdx)
                                    * FluidSystem::decayRate(compIdx)
                                    * volVars.molarDensity(compIdx)
                                    * volVars.porosity()
                                    * volVars.saturation(0)
                                 : - volVars.massFraction(0, compIdx)
                                    * FluidSystem::decayRate(compIdx)
                                    * FluidSystem::molarMass(compIdx)
                                    * volVars.molarDensity(compIdx)
                                    * volVars.porosity()
                                    * volVars.saturation(0);
            if(useKdModel_)
                qRadDecay[compIdx] += useMoles ? - volVars.moleFraction(0, compIdx)
                                    * FluidSystem::decayRate(compIdx)
                                    * FluidSystem::kdValue(compIdx)
                                    * volVars.molarDensity(compIdx)
                                    * solidBulkDensity_
                                 : - volVars.massFraction(0, compIdx)
                                    * FluidSystem::kdValue(compIdx)
                                    * FluidSystem::decayRate(compIdx)
                                    * FluidSystem::molarMass(compIdx)
                                    * volVars.molarDensity(compIdx)
                                    * solidBulkDensity_;

            if (FluidSystem::decayDaughter(compIdx) != "stable") //checking if component has a daughter
            {
                unsigned int compDIdx = getDaughterName(FluidSystem::decayDaughter(compIdx)); //name and index of the component are determined
                qRadDecay[compDIdx] += -qRadDecay[compIdx];

                const unsigned int eqDIdx = conti0EqIdx + compDIdx;
                source[eqDIdx] += qRadDecay[compDIdx];
            }

            const unsigned int eqIdx = conti0EqIdx + compIdx;
            source[eqIdx] += qRadDecay[compIdx];
        }

        return source;
    }

    /*!
     * \brief Determines the name of the daughter after the radioactive decay.
     * The name of the daughter is compared with the component name in the system.
     *
     * \param daughter Name of the daughter
     */
    int getDaughterName(const std::string daughter) const
    {
        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
        {
            if(FluidSystem::componentName(compIdx) == daughter)
                return compIdx;
            else
                continue;
        }
        DUNE_THROW(Dune::InvalidStateException, "Invalid daughter name: " << daughter);
    }

    /*!
     * \brief Appends all quantities of interest which can be derived
     *        from the solution of the current time step to the VTK
     *        writer.
     */

    template<class VTKWriter>
    void addFieldsToWriter(VTKWriter& vtk)
    {
        const auto numDofs = this->gridGeometry().numDofs();

        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
        {
            if(!FluidSystem::isRadioactive(compIdx))
                continue;
            vtkBqPerL_.resize(numComponents-1, std::vector<Scalar>(numDofs));
            vtk.addField(vtkBqPerL_[compIdx], "BqPerL_" + FluidSystem::componentName(compIdx));
            vtkBqPerG_.resize(numComponents-1, std::vector<Scalar>(numDofs));
            vtk.addField(vtkBqPerG_[compIdx], "BqPerG_" + FluidSystem::componentName(compIdx));
        }
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter.
     *
     * Function is called by the output module on every write.
     */
    void updateVtkFields(const TracerSolutionVector& curSol)
    {
        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
        {
            if(!FluidSystem::isRadioactive(compIdx))
                continue;

            for (const auto& element : elements(this->gridGeometry().gridView()))
            {
                auto elemSol = elementSolution(element, curSol, this->gridGeometry());
                auto fvGeometry = localView(this->gridGeometry());
                fvGeometry.bindElement(element);

                for (auto&& scv : scvs(fvGeometry))
                {
                    const auto dofIdxGlobal = scv.dofIndex();
                    VolumeVariables volVars;
                    volVars.update(elemSol, *this, element, scv);
                    vtkBqPerL_[compIdx][dofIdxGlobal] = volVars.moleFraction(0, compIdx) //[mol R/mol W]
                                                        * FluidSystem::specificActivity(compIdx) //[Bq/mol R]
                                                        * volVars.molarDensity(compIdx) //[mol W/m^3 W]
                                                        * 0.001 // [m^3/l]
                                                        / 1e10;
                    vtkBqPerG_[compIdx][dofIdxGlobal] = volVars.moleFraction(0, compIdx) //[mol R/mol W]
                                                        * FluidSystem::specificActivity(compIdx) //[Bq/mol R]
                                                        * volVars.molarDensity(compIdx) //[mol W/m^3 W]
                                                        * FluidSystem::kdValue(compIdx) //[m^3 W/kg S]
                                                        * 0.001 //[kg/g]
                                                        / 1e10;
                }
            }
        }
    }

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        if(onRightBoundary_(globalPos))
           values.setAllDirichlet();
        else
            values.setAllNeumann();

        return values;
    }
    // \}

    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment.
     *
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);

        return values;
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        if (onRightBoundary_(globalPos))
            values = 0.0;

        return values;
    }

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables initialValues(0.0);
        if(isLandfill_(globalPos))
        {
            initialValues[conti0EqIdx] = initialValue_ //[Bq R/g S]
                                        / FluidSystem::specificActivity(conti0EqIdx) //[Bq R/mol R]
                                        / 55555.6 // volVars.molarDensity(compIdx) //[mol W/m^3 W]
                                        / FluidSystem::kdValue(conti0EqIdx) //[m^3 W/kg S]
                                        / (1-0.46)
                                        * 1000 //[g/kg]
                                        * 1e10;
        }
        return initialValues;
    }

    /*!
     * \brief Returns the time for a variable neumann boundary condition.
     *
     */
    void setTime(Scalar time)
    {
        time_ = time;
    }

private:
    static constexpr Scalar eps_ = 1e-6;
    Scalar deltaH_;
    bool useKdModel_;
    Scalar time_, timeStepSize_;

    Scalar initialValue_, solidBulkDensity_;
    std::vector<std::vector<Scalar>> vtkBqPerL_, vtkBqPerG_;

    bool isLandfill_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] > 15 - globalPos[0]*deltaH_ - eps_;
    }

    bool isAquifer_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] < 10 - globalPos[0]*deltaH_ + eps_;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] < this->gridGeometry().bBoxMin()[dim-1] - globalPos[0]*deltaH_ + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] > this->gridGeometry().bBoxMax()[dim-1] - globalPos[0]*deltaH_ - eps_;
    }
};

} //end namespace Dumux

#endif
