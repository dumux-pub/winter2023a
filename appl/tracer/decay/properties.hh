// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup TracerTests
 * \brief The properties for the tracer problem:
 * A rotating velocity field mixes a tracer band in a porous groundwater reservoir.
 */
#ifndef DUMUX_TRACER_DECAY_TEST_PROPERTIES_HH
#define DUMUX_TRACER_DECAY_TEST_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>
#include <dumux/porousmediumflow/tracer/model.hh>

#include "spatialparams.hh"
#include "dumux/material/fluidsystems/fluidsystem_tracer_i129.hh"

#ifndef USEMOLES // default to true if not set through CMake
#define USEMOLES true
#endif

#include "problem.hh"

namespace Dumux::Properties {
// Create new type tags
namespace TTag {
struct TracerDecayTest { using InheritsFrom = std::tuple<Tracer>; };
struct TracerDecayTestTpfa { using InheritsFrom = std::tuple<TracerDecayTest, CCTpfaModel>; };
struct TracerDecayTestMpfa { using InheritsFrom = std::tuple<TracerDecayTest, CCMpfaModel>; };
struct TracerDecayTestBox { using InheritsFrom = std::tuple<TracerDecayTest, BoxModel>; };
} // end namespace TTag

// enable caching
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::TracerDecayTest> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::TracerDecayTest> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::TracerDecayTest> { static constexpr bool value = true; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TracerDecayTest> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::TracerDecayTest> { using type = TracerDecayTest<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TracerDecayTest>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = TracerDecayTestSpatialParams<GridGeometry, Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::TracerDecayTest> { static constexpr bool value = USEMOLES; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TracerDecayTest> { using type = FluidSystems::TracerFluidSystem<TypeTag>; };

} // end namespace Dumux::Properties

#endif
