// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/**
 * \file
 * \ingroup TracerTests
 * \brief Definition of a problem for the tracer problem:
 * A rotating velocity field mixes a tracer band in a porous groundwater reservoir.
 */

#ifndef DUMUX_TRACER_TEST_PROBLEM_HH
#define DUMUX_TRACER_TEST_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>

#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

/*!
 * \ingroup TracerTests
 *
 * \brief Definition of a problem for the tracer problem:
 * A lens of contaminant tracer is diluted by diffusion and a base groundwater flow
 *
 * This problem uses the \ref TracerModel model.
 */
template <class TypeTag>
class TracerDecayTest : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;

    //! property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();
    static constexpr int numComponents = FluidSystem::numComponents;
    enum
    {
        conti0EqIdx = Indices::conti0EqIdx,
    };

public:
    TracerDecayTest(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        initConc_ = getParam<Scalar>("Problem.InitialConcentration");
        // stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout<<"problem uses mole fractions" << '\n';
        else
            std::cout<<"problem uses mass fractions" << '\n';

        hasRetardation_ = getParam<bool>("Problem.HasRetardation", false);
    }

    /*
    * States the used Sorption model
    */
    bool hasRetardation() const
    { return hasRetardation_; }

    /*!
     * \brief Evaluates the source term at a given position.
     * The radioactive decay is given as source term
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scvf The sub-control volume faceolVars.massFraction(
     */
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);
        const auto& volVars = elemVolVars[scv];

        std::array<Scalar, numComponents> qRadDecay = {0.0};
        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
        {
            if (!FluidSystem::isRadioactive(compIdx))
                continue;

            qRadDecay[compIdx] = useMoles ? - volVars.moleFraction(0, compIdx)
                                    * FluidSystem::decayRate(compIdx)
                                    * volVars.molarDensity(compIdx)
                                    * volVars.porosity()
                                    //*saturation(0)
                                 : - volVars.massFraction(0, compIdx)
                                    * FluidSystem::decayRate(compIdx)
                                    * FluidSystem::molarMass(compIdx)
                                    * volVars.molarDensity(compIdx)
                                    * volVars.porosity();
                                    //*saturation(0);

            if (FluidSystem::decayDaughter(compIdx) != "stable") //checking if component has a daughter
            {
                unsigned int compDIdx = getDaughterName(FluidSystem::decayDaughter(compIdx)); //name and index of the component are determined
                qRadDecay[compDIdx] = -qRadDecay[compIdx];
                const unsigned int eqDIdx = conti0EqIdx + compDIdx;
                source[eqDIdx] += qRadDecay[compDIdx];
            }

            const unsigned int eqIdx = conti0EqIdx + compIdx;
            source[eqIdx] += qRadDecay[compIdx];

            using std::isfinite;
            if (!isfinite(source[eqIdx]))
                DUNE_THROW(NumericalProblem, "Calculated non-finite source");
        }
        return source;
    }

    /*!
     * \brief Determines the name of the daughter after the radioactive decay.
     * The name of the daughter is compared with the component name in the system.
     *
     * \param daughter Name of the daughter
     */
    int getDaughterName(const std::string daughter) const
    {
        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
        {
            if(FluidSystem::componentName(compIdx) == daughter)
                return compIdx;
            else
                continue;
        }
        DUNE_THROW(Dune::InvalidStateException, "Invalid daughter name: " << daughter);
    }

    /*!
     * \brief Appends all quantities of interest which can be derived
     *        from the solution of the current time step to the VTK
     *        writer.
     */

    template<class VTKWriter>
    void addFieldsToWriter(VTKWriter& vtk)
    {
        const auto numDofs = this->gridGeometry().numDofs();
        vtkBqPerKg_.resize(numDofs, 0.0);
        vtk.addField(vtkBqPerKg_, "BqPerKg");

        vtkAnalyticSol1_.resize (numDofs, 0.0);
        vtk.addField(vtkAnalyticSol1_, "AnalyticSol1");
        vtkAnalyticSol2_.resize (numDofs, 0.0);
        vtk.addField(vtkAnalyticSol2_, "AnalyticSol2");
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter.
     *
     * Function is called by the output module on every write.
     */
    void updateVtkFields(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto elemSol = elementSolution(element, curSol, this->gridGeometry());
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto dofIdxGlobal = scv.dofIndex();
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);

                for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                    if (!FluidSystem::isRadioactive(compIdx))
                        continue;
                    vtkBqPerKg_[dofIdxGlobal] = 0.0;
                    vtkBqPerKg_[dofIdxGlobal] += volVars.massFraction(0, compIdx) * FluidSystem::specificActivity(compIdx);
                }
                vtkAnalyticSol1_[dofIdxGlobal] = initConc_ * exp(-FluidSystem::decayRate(0) * time_);
                vtkAnalyticSol2_[dofIdxGlobal] = 0.0;/*initConc_ * FluidSystem::decayRate(0)
                                                    *((exp(-FluidSystem::decayRate(0) * time_)
                                                    /(FluidSystem::decayRate(1) - FluidSystem::decayRate(0)))
                                                    +(exp(-FluidSystem::decayRate(1) * time_)
                                                    /(FluidSystem::decayRate(0) - FluidSystem::decayRate(1))));
            */}
        }
    }


    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann(); // no-flow
        return values;
    }
    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables initialValues(0.0);

        if (useMoles)
            initialValues[0] = initConc_;
        else
            initialValues[0] = initConc_*FluidSystem::molarMass(0)/this->spatialParams().fluidMolarMass(globalPos);
        return initialValues;
    }

    void setTime(Scalar time)
    { time_ = time; }

    // \}

private:
    static constexpr Scalar eps_ = 1e-6;

    Scalar time_;
    Scalar initConc_;
    bool hasRetardation_;

    std::vector<Scalar> vtkBqPerKg_;
    std::vector<Scalar> vtkAnalyticSol1_;
    std::vector<Scalar> vtkAnalyticSol2_;
};

} // end namespace Dumux

#endif
