// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup RichardsTests
 * \brief Spatial parameters for the RichardsRadioactiveTestProblem.
 */

#ifndef DUMUX_RICHARDS_RADIOACTIVETEST_SPATIAL_PARAMS_HH
#define DUMUX_RICHARDS_RADIOACTIVETEST_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/porousmediumflow/richards/model.hh>
#include <dumux/material/fluidmatrixinteractions/2p/brookscorey.hh>

namespace Dumux {

/*!
 * \ingroup RichardsTests
 * \brief The spatial parameters for the RichardsNCRadioactiveTestProblem.
 */
template<class GridGeometry, class Scalar>
class RichardsRadioactiveTestSpatialParams
: public FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, RichardsRadioactiveTestSpatialParams<GridGeometry, Scalar>>
{
    using ThisType = RichardsRadioactiveTestSpatialParams<GridGeometry, Scalar>;
    using ParentType = FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, ThisType>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
public:
    using PcKrSwCurve = FluidMatrix::BrooksCoreyDefault<Scalar>;
    using PermeabilityType = Scalar;


    RichardsRadioactiveTestSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    , n_(gridGeometry->gridView().size(0), 0.0)
    , K_(gridGeometry->gridView().size(0), 0.0)
    , entryPressure_(gridGeometry->gridView().size(0), 0.0)
    , globalPos_(gridGeometry->gridView().size(0))
    {
        porosity_ = getParam<Scalar>("SpatialParams.N", 0.35);
        permeability_ = getParam<Scalar>("SpatialParams.K", 1e-11);

        PcEInit_ = getParam<Scalar>("SpatialParams.BrooksCoreyPcEntry", 1e3);
        swr_ = getParam<Scalar>("SpatialParams.Swr", 0.0);
        snr_ = getParam<Scalar>("SpatialParams.Snr", 0.0);
        lambda_ = getParam<Scalar>("SpatialParams.BrooksCoreyLambda", 2.0);

        bool lens = getParam<bool>("SpatialParams.lens", false);
        std::vector<double> lens1 = getParam<std::vector<double>>("SpatialParams.Lens1");
        Scalar lens1Poro = getParam<Scalar>("SpatialParams.Lens1Poro");
        std::vector<double> lens2 = getParam<std::vector<double>>("SpatialParams.Lens2");
        Scalar lens2Poro = getParam<Scalar>("SpatialParams.Lens2Poro");

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto eIdx = this->gridGeometry().elementMapper().index(element);
            globalPos_[eIdx] =  element.geometry().center();
        }


        //reading porosity and permeability from an external created file
        std::string poroDataFile = getParam<std::string>("SpatialParams.poroFile");

        std::ifstream poroFile(poroDataFile);
        if (!poroFile.good())
        {
            DUNE_THROW(Dune::IOError, "Reading from file: "<< poroDataFile << " failed." << std::endl);
        }
        std::string line;
        std::getline(poroFile, line);

        Scalar trash, poroDataValue;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            std::getline(poroFile, line);
            std::istringstream curLine(line);
            auto eIdx = this->gridGeometry().elementMapper().index(element);

            if (dim == 1)
                curLine >> trash >> trash >> poroDataValue;
            else if (dim == 2)
                curLine >> trash >> trash >> poroDataValue;
            else if (dim == 3)
                curLine >> trash >> trash >> trash >> poroDataValue;
            else
                DUNE_THROW(Dune::InvalidStateException, "Invalid dimension " << dim);

            if (lens == true)
            {
                auto x = globalPos_[eIdx][0];
                auto y = globalPos_[eIdx][1];
                if ( lens1[0] < x && x < lens1[1] && lens1[2] < y && y < lens1[3])
                {
                    n_[eIdx] = lens1Poro;
                    K_[eIdx] = permeability_ * (std::pow(lens1Poro, 3) * std::pow(1-porosity_, 2))/(std::pow(porosity_, 3) * std::pow(1-lens1Poro, 2));
                }
                else if ( lens2[0] < x && x < lens2[1] && lens2[2] < y && y < lens2[3])
                {
                    n_[eIdx] = lens2Poro;
                    K_[eIdx] = permeability_ * (std::pow(lens2Poro, 3) * std::pow(1-porosity_, 2))/(std::pow(porosity_, 3) * std::pow(1-lens2Poro, 2));
                }
                else
                {
                    n_[eIdx] = porosity_* std::pow(10, (poroDataValue));
                    K_[eIdx] = permeability_ * (std::pow(n_[eIdx], 3) * std::pow(1-porosity_, 2))/(std::pow(porosity_, 3) * std::pow(1-n_[eIdx], 2));
                }
            }
            else
            {
                n_[eIdx] = porosity_* std::pow(10, (poroDataValue));
                K_[eIdx] = permeability_ * (std::pow(n_[eIdx], 3) * std::pow(1-porosity_, 2))/(std::pow(porosity_, 3) * std::pow(1-n_[eIdx], 2));
            }
            // Leverett after Saadatpoor, 2009, Effect of capillary heterogeneity
            entryPressure_[eIdx]= PcEInit_* std::sqrt(permeability_ * porosity_ / (K_[eIdx]*n_[eIdx]));
        }
    }

    //! get the permeability field for output
    const std::vector<Scalar>& getPermField() const
    {return K_; }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *        In this test, we use element-wise distributed permeabilities.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    { return K_[scv.dofIndex()]; }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The porosity
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    { return n_[scv.dofIndex()]; }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The current element
     * \return The porosity
     */
    Scalar porosity(const Element& element) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        return n_[eIdx];
    }

    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law.
     *
     * In this test, we use element-wise distributed material parameters.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The material parameters object
     */
    template<class ElementSolution>
    auto fluidMatrixInteraction(const Element& element,
                                const SubControlVolume& scv,
                                const ElementSolution& elemSol) const
    {
        if (porosity(element) > 0.90 - eps_)
        {
            typename PcKrSwCurve::BasicParams params(PcEInit_, lambda_);
            typename PcKrSwCurve::EffToAbsParams effToAbsParams(0.0, snr_);
            return makeFluidMatrixInteraction(PcKrSwCurve(params, effToAbsParams));
        }
        else
        {
            typename PcKrSwCurve::BasicParams params(entryPressure_[scv.dofIndex()], lambda_);
            typename PcKrSwCurve::EffToAbsParams effToAbsParams(swr_, snr_);
            return makeFluidMatrixInteraction(PcKrSwCurve(params, effToAbsParams));
        }
    }

    /*!
     * \brief Returns the fluid-matrix interaction law an element
     *
     * \param element The current finite element
     * \return The material parameters object
     */
    auto fluidMatrixInteraction(const Element& element) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        //const auto& globalPos = globalPos_[eIdx];

        if (n_[eIdx] > 0.90 - eps_)
        {
            typename PcKrSwCurve::BasicParams params(PcEInit_, lambda_);
            typename PcKrSwCurve::EffToAbsParams effToAbsParams(0.0, snr_);
            return makeFluidMatrixInteraction(PcKrSwCurve(params, effToAbsParams));
        }
        else
        {
            typename PcKrSwCurve::BasicParams params(entryPressure_[eIdx], lambda_);
            typename PcKrSwCurve::EffToAbsParams effToAbsParams(swr_, snr_);
            return makeFluidMatrixInteraction(PcKrSwCurve(params, effToAbsParams));
        }
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \param globalPos The global position
     * \return The wetting phase index
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }

private:
    Scalar porosity_;
    Scalar permeability_;

    Scalar PcEInit_;
    Scalar swr_;
    Scalar snr_;
    Scalar lambda_;

    std::vector<Scalar> n_;
    std::vector<Scalar> K_;
    std::vector<Scalar> entryPressure_;
    std::vector<GlobalPosition> globalPos_;

    static constexpr Scalar eps_ = 1.5e-7;
};

} // end namespace Dumux

#endif
