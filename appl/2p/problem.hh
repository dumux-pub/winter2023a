// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup TwoPTests
 * \brief
 */
#ifndef DUMUX_2P_RADIOACTIVETEST_PROBLEM_HH
#define DUMUX_2P_RADIOACTIVETEST_PROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>
#include <dumux/porousmediumflow/2p/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

#include "spatialparams.hh"
#include <dumux/flux/maxwellstefanslaw.hh>

#if FORCH_CONST
#include <dumux/flux/forchheimerslaw.hh> // forchheimer solution with constant coefficient
#elif FORCH_MP
#include <dumux/flux/forchheimerslaw_mp.hh> // forchheimer solution with calculated coefficient
#endif

namespace Dumux {

template <class TypeTag>
class TwoPRadioactiveTestProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct TwoPRadioactiveTest { using InheritsFrom = std::tuple<TwoP>; };
struct TwoPRadioactiveTestBox { using InheritsFrom = std::tuple<TwoPRadioactiveTest, BoxModel>; };
struct TwoPRadioactiveTestCCTpfa { using InheritsFrom = std::tuple<TwoPRadioactiveTest, CCTpfaModel>; };
struct TwoPRadioactiveTestCCMpfa { using InheritsFrom = std::tuple<TwoPRadioactiveTest, CCMpfaModel>; };
} // end namespace TTag

// Specialize the fluid system type for this type tag
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPRadioactiveTest>
{
    using Scalar = GetPropType<TypeTag, Scalar>;
    using type = FluidSystems::H2OAir<Scalar, Components::SimpleH2O<Scalar> >;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPRadioactiveTest> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPRadioactiveTest> { using type = TwoPRadioactiveTestProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPRadioactiveTest>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = TwoPRadioactiveTestSpatialParams<GridGeometry, Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::TwoPRadioactiveTest> { static constexpr bool value = true; };

//! Here we set FicksLaw or TwoPDiffusionsLaw
template<class TypeTag>
struct MolecularDiffusionType<TypeTag, TTag::TwoPRadioactiveTest> { using type = FicksLaw<TypeTag>; };

//! Set the default formulation to pw-Sn: This can be over written in the problem.
template<class TypeTag>
struct Formulation<TypeTag, TTag::TwoPRadioactiveTest>
{ static constexpr auto value = TwoPFormulation::p0s1; };

#if defined(FORCH_CONST) || defined(FORCH_MP)
// Specialize the advection type for this type tag
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::TwoPRadioactiveTest>
{using type = ForchheimersLaw<TypeTag>; };
#endif

} // end namespace Properties

/*!
 * \ingroup TwoPTests
 * \brief
 */
template <class TypeTag>
class TwoPRadioactiveTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using TimeLoop = TimeLoopBase<Scalar>;

    enum {
        // copy some indices for convenience
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx,

        // component indices
        H2OIdx = FluidSystem::H2OIdx,
        AirIdx = FluidSystem::AirIdx,

        // equation indices
        conti0EqIdx = Indices::conti0EqIdx + H2OIdx,
        contiAirEqIdx = conti0EqIdx + AirIdx,
    };

    //! Property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();

public:
    TwoPRadioactiveTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry) {
         injectionMass_ = getParam<Scalar>("Problem.InjectionMass", -0.0);
    }

	Scalar forchConst(const SubControlVolumeFace scvf) const
    {
    	Scalar forchConst = 0.55;
     	return forchConst;
    }

    Scalar forchCoeff(const ElementVolumeVariables elemVolVars,
                      const SubControlVolumeFace scvf,
                      int phaseIdx) const
    {
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];

        auto satReg = std::max(volVars.saturation(phaseIdx), 1e-4);
        //parameter for the calculation of forchCoeff
		const Scalar C_beta = 1.52e-4; //[m] (Zhang, 2013)
		const Scalar phi = volVars.porosity();
		const Scalar tau = 1/phi;
	    // calculation of the coefficient
		Scalar forchCoeff = (C_beta*tau)/(phi*satReg);

        return forchCoeff;
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        if (onLowerBoundary_(globalPos))
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann
     *        boundary segment in dependency on the current solution.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub-control volume face
     *
     * This method is used for cases, when the Neumann condition depends on the
     * solution and requires some quantities that are specific to the fully-implicit method.
     * The \a values store the mass flux of each phase normal to the boundary.
     * Negative values indicate an inflow.
     */
    template<class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        const auto& globalPos = scvf.ipGlobal();
        if (onUpperBoundary_(globalPos))
        {
            values[conti0EqIdx] = injectionMass_;
        }
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param element The finite element
     */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolumeFace &scvf) const
    {
        PrimaryVariables values(0.0);

        const auto& globalPos = scvf.ipGlobal();
        const Scalar sw = 0.1;
        const Scalar pc = this->spatialParams().fluidMatrixInteraction(element).pc(sw);

        if (onLowerBoundary_(globalPos))
        {
            values[pressureIdx] = 1e5 - pc;
            values[saturationIdx] = 1.0 - sw;
        }

        return values;
    }

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param element The finite element
     */
    PrimaryVariables initial(const Element& element) const
    {
        PrimaryVariables values(0.0);

        const Scalar sw = 0.1;
        const Scalar pc = this->spatialParams().fluidMatrixInteraction(element).pc(sw);

        values[pressureIdx] = 1e5 - pc;
        values[saturationIdx] = 1.0 - sw;

        return values;
    }

    /*!
     * \brief Returns the time for a variable neumann boundary condition.
     *
     */
    void setTime(Scalar time)
    {
        time_ = time;
    }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_;
    }

    static constexpr Scalar eps_ = 1e-6;

    Scalar time_;
    Scalar injectionMass_;

    std::vector<Scalar> K_;
    std::vector<Scalar> vtkK_;
};

} // end namespace Dumux

#endif
