// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Test for the landfill grid manager
 */

#include<string>

#include "config.h"
#include <iostream>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/landfillgridmanager.hh>

#if HAVE_DUNE_UGGRID
#include <dune/grid/uggrid.hh>
#endif

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif

#ifndef USEUG
#define USEUG true
#endif

// The grid type
#if HAVE_DUNE_UGGRID && USEUG==1
template<int dim>
using Grid = Dune::UGGrid<dim>;
#elif HAVE_DUNE_ALUGRID
template<int dim>
using Grid = Dune::ALUGrid<dim, dim, Dune::cube, Dune::nonconforming>;
#endif

template<int dim>
void testLandfillGridManager(const std::string& name)
{
    // using declarations
    using GridManager = typename Dumux::LandfillGridManager<Grid<dim>>;
    GridManager gridManager;

    // make the grid
    Dune::Timer timer;
    gridManager.init();
    const auto& gridView = gridManager.grid().leafGridView();
    if (gridView.comm().rank() == 0)
        std::cout << "Constructing " << dim << "-d landfill grid with " << gridManager.grid().leafGridView().size(0) << " elements took "
              << timer.elapsed() << " seconds.\n";
    // construct a vtk output writer and attach the boundaryMakers
    Dune::VTKWriter<typename Grid<dim>::LeafGridView> vtkWriter(gridManager.grid().leafGridView());
    vtkWriter.write(name);
}

int main(int argc, char** argv)
{
    // initialize MPI, finalize is done automatically on exit
    Dune::MPIHelper::instance(argc, argv);

    // first read parameters from input file
    Dumux::Parameters::init(argc, argv, "test_gridmanager_landfill.input");
    const auto name = Dumux::getParam<std::string>("Grid.Name");

    // test 3-D
    testLandfillGridManager<3>("landfill_3d-" + name);

    return 0;
}
