add_subdirectory(tracer)
add_subdirectory(tracermin)
add_subdirectory(tracermineralization)

file(GLOB DUMUX_POROUSMEDIUMFLOW_HEADERS *.hh *.inc)
install(FILES ${DUMUX_POROUSMEDIUMFLOW_HEADERS}
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/porousmediumflow)
