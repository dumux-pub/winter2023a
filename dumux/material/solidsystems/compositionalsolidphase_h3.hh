// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup SolidSystems
 * \brief A solid phase consisting of multiple inert solid components.
 */
#ifndef DUMUX_SOLIDSYSTEMS_COMPOSITIONAL_SOLID_PHASE_HH
#define DUMUX_SOLIDSYSTEMS_COMPOSITIONAL_SOLID_PHASE_HH

#include <string>
#include <dune/common/exceptions.hh>

namespace Dumux {
namespace SolidSystems {

/*!
 * \ingroup SolidSystems
 * \brief A solid phase consisting of multiple inert solid components.
 * \note a solid is considered inert if it cannot dissolve in a liquid and
 *       and cannot increase its mass by precipitation from a fluid phase.
 * \note inert components have to come after all non-inert components
 */

template <class Scalar, int numInert = 0>
class CompositionalSolidPhase
{
public:

    /****************************************
     * Solid phase related static parameters
     ****************************************/
    static constexpr int numComponents = 3;
    static constexpr int numInertComponents = numInert;
    static constexpr int comp0Idx = 0;
    static constexpr int comp1Idx = 1;
    static constexpr int comp2Idx = 2;

   //! Human readable component name (index compIdx) (for vtk output)
    static std::string componentName(int compIdx)
    {
        static std::string name[] = {
            "H3",
            "He3",
            "Granite"
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return name[compIdx];
    }

    /*!
     * \brief A human readable name for the solid system.
     */
    static std::string name()
    { return "s"; }

    /*!
     * \brief Returns whether the phase is incompressible
     */
    static constexpr bool isCompressible(int compIdx)
    { return false; }

    /*!
     * \brief Returns whether the component is inert (doesn't react)
     */
    static constexpr bool isInert()
    { return (numComponents == numInertComponents); }

    /*!
     * \brief Return Molar mass of the component in \f$\mathrm{\frac{[kg]}{[mol]}}\f$.
     * \param compIdx The index of the component to consider
     */
    static Scalar molarMass(int compIdx)
    {
        static const Scalar M[] = {
            0.003,
            0.003,
            0.6008
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return M[compIdx];
    }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar density(const SolidState& solidState)
    {
        return 2700;
    }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar density(const SolidState& solidState, const int compIdx)
    {
       static const Scalar M[] = {
            2700,
            2700,
            2700
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return M[compIdx];
    }

    /*!
     * \brief The molar density of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar molarDensity(const SolidState& solidState, const int compIdx)
    {
        static const Scalar M[] = {
            2700/molarMass(compIdx),
            2700/molarMass(compIdx),
            2700/molarMass(compIdx)
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return M[compIdx];
    }

    /*!
     * \brief Returns true if the component is radioactive
     * \param compIdx The index of the component to consider
     */
    static bool isRadioactive(int compIdx)
    {
        static constexpr bool iR[] = {
            true,
            false,
            false
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return iR[compIdx];
    }

    /*!
     * \brief Return the half-life of a radioactive component in \f$\mathrm{[s]}\f$.
     * \param compIdx The index of the component to consider
     */
    static Scalar halfLife(int compIdx)
    {
        static const Scalar HL[] = {
            388523520,
            1e300,
            1e300
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return HL[compIdx];
    }

    /*!
     * \brief Return the daughter component of the radioactive decay
     * \param compIdx The index of the component to consider
     */
    static std::string decayDaughter(int compIdx)
    {
        static std::string dname[] = {
            "He3",
            "stable",
            "stable"
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return dname[compIdx];
    }

    /*!
     * \brief Return the specific activity of a radioactive component in \f$\mathrm{[Bq/kg]}\f$.
     * \param compIdx The index of the component to consider
     */
    static Scalar specificActivity(int compIdx)
    {
        return log(2) / halfLife(compIdx) * 6.02214076e23 / molarMass(compIdx);
    }

    /*!
     * \brief Calculation of decay rate of a component
     * Used is the usual radioactive decay equation N = N*exp(-t/lambda).
     * Here the derivation of this equation is used, also possible are two other formulations:
     * (1 - std::pow(2,(-timeStepSize/halfLife)))/timeStepSize;
     * (1 - exp(-log(2)/halfLife * timeStepSize))/timeStepSize
     * \param compIdx The index of the component to consider
     */
    static Scalar decayRate(int compIdx)
    {
        return log(2)/halfLife(compIdx);
    }
};

} // end namespace SolidSystems
} // end namespace Dumux

#endif
