// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup Components
 * \brief A class for the radioactive Iod129 component properties
 */
#ifndef DUMUX_IOD_HH
#define DUMUX_IOD_HH

#include <dumux/material/components/base.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for the radioactive Iod129 component properties.
 */
template <class Scalar>
class Iod129
: public Components::Base<Scalar, Iod129<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the Iod ion.
     */
    static std::string name()
    { return "I129"; }

    /*!
     * \brief Returns true if the component is radioactive
     */
    static constexpr bool isRadioactive()
    { return true; }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of the Iod ion.
     */
    static constexpr Scalar molarMass()
    { return 129e-3; } // kg/mol

    /*!
     * \brief The half-life of the Iod ion in \f$\mathrm{[s]}\f$.
     */
    static constexpr Scalar halfLife()
    { return 5.045e14; }

    /*!
     * \brief The kd-value of the Iod ion in \f$\mathrm{[m^3/kg]}\f$.
     */
    static constexpr Scalar kdValue()
    { return 0.001; }

    /*!
     * \brief Return the theoretical specific activity of a radioactive component in \f$\mathrm{[Bq/kg]}\f$,
     * calculated with the half-life.
     */
    static constexpr Scalar specificActivity()
    { return log(2) / halfLife() * 6.02214076e23 / molarMass(); }

    /*!
     * \brief The daughter of the radioactive decay.
     */
    static std::string decayDaughter()
    { return "stable"; }
};

} // end namespace Components
} // end namespace Dumux

#endif
