// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup Components
 * \brief Interface for components that are radioactive.
 */
#ifndef DUMUX_COMPONENT_RADIOACTIVE_HH
#define DUMUX_COMPONENT_RADIOACTIVE_HH

#include <dune/common/exceptions.hh>
#include <dumux/material/components/base.hh>

#include <dumux/common/typetraits/typetraits.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief Interface for components that are radioactive.
 */
template<class Scalar, class Component>
class Radioactive
{
public:
    /*!
     * \brief Returns true if the component is radioactive
     */
    template<class C = Component>
    static constexpr bool isRadioactive()
    {
        static_assert(AlwaysFalse<C>::value, "Mandatory function not implemented: isRadioactive()");
        return 0; // iso c++ requires a return statement for constexpr functions
    }

    /*!
     * \brief The half-life in \f$\mathrm{[s]}\f$ of the component.
     */
    template<class C = Component>
    static constexpr Scalar halfLife()
    {
        static_assert(AlwaysFalse<C>::value, "Mandatory function not implemented: halfLife()");
        return 0; // iso c++ requires a return statement for constexpr functions
    }

    /*!
     * \brief The kd-value in \f$\mathrm{[m^3/kg]}\f$.
     */
    template<class C = Component>
    static constexpr Scalar kdValue()
    {
        static_assert(AlwaysFalse<C>::value, "Mandatory function not implemented: kdValue()");
        return 0; // iso c++ requires a return statement for constexpr functions
    }

    /*!
     * \brief The daughter of the radioactive decay.
     */
    template<class C = Component>
    static std::string decayDaughter()
    {
        static_assert(AlwaysFalse<C>::value, "Mandatory function not implemented: decayDaughter()");
        return 0; // iso c++ requires a return statement for constexpr functions
    }
};

} // end namespace Components
} // end namespace Dumux

#endif
