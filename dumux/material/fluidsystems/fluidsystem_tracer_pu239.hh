// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup Fluidsystems
 * \brief
 */
#ifndef DUMUX_RAD_TRACER_FLUID_SYSTEM_HH
#define DUMUX_RAD_TRACER_FLUID_SYSTEM_HH

#include <dumux/material/fluidsystems/base.hh>

namespace Dumux {
namespace FluidSystems {

//! A simple fluid system with one tracer component
template<class TypeTag>
class TracerFluidSystem : public FluidSystems::Base<GetPropType<TypeTag, Properties::Scalar>,
                                                                TracerFluidSystem<TypeTag>>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

public:
    //! If the fluid system only contains tracer components
    static constexpr bool isTracerFluidSystem()
    { return true; }

    //! No component is the main component
    static constexpr int getMainComponent(int phaseIdx)
    { return 0; }

    //! The number of components
    static constexpr int numComponents = 11;

    //! Human readable component name (index compIdx) (for vtk output)
    static std::string componentName(int compIdx)
    {
        static std::string name[] = {
            "Pu239",
            "U235",
            "Th231",
            "Pa231",
            "Ac227",
            "Th227",
            "Ra223",
            "Pb211",
            "Bi211",
            "Tl207",
            "Pb207"
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return name[compIdx];
    }

    //! Molar mass in kg/mol of the component with index compIdx
    static Scalar molarMass(int compIdx)
    {
        static const Scalar M[] = {
            0.239,
            0.235,
            0.231,
            0.231,
            0.227,
            0.227,
            0.223,
            0.211,
            0.211,
            0.207,
            0.207
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return M[compIdx];
    }

    /*!
     * \brief Returns true if the component is radioactive
     * \param compIdx The index of the component to consider
     */
    static bool isRadioactive(int compIdx)
    {
        static constexpr bool iR[] = {
            true,
            true,
            true,
            true,
            true,
            true,
            true,
            true,
            true,
            true,
            false
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return iR[compIdx];
    }

    /*!
     * \brief Return the half-life of a radioactive component in \f$\mathrm{[s]}\f$.
     * \param compIdx The index of the component to consider
     */
    static Scalar halfLife(int compIdx)
    {
        static const Scalar HL[] = {
            760332960000,
            22200000000000825.6,
            86400,
            1033119360000,
            686601792,
            1614000,
            987600,
            2170,
            128.4,
            286,
            1e300
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return HL[compIdx];
    }

    /*!
     * \brief Return the daughter component of the radioactive decay
     * \param compIdx The index of the component to consider
     */
    static std::string decayDaughter(int compIdx)
    {
        static std::string dname[] = {
            "U235",
            "Th231",
            "Pa231",
            "Ac227",
            "Th227",
            "Ra223",
            "Pb211",
            "Bi211",
            "Tl207",
            "Pb207",
            "stable"
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return dname[compIdx];
    }

    /*!
     * \brief The kd-value in \f$\mathrm{[m^3/kg]}\f$.
     */
    static Scalar kdValue(int compIdx)
    {
        static const Scalar kd[] = {
            /*0.001 to 5.2: range of Kd values in Handford GW samples, Cantrell, Serne, Last, 2003,
            Handford Contaminant Distribution Coefficient Database and Users Guide*/
            0.001,
            0.001,
            0.001,
            0.001,
            0.001,
            0.001,
            0.001,
            0.001,
            0.001,
            0.001,
            0.001
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return kd[compIdx];
    }

    /*!
     * \brief Return the specific activity of a radioactive componet in \f$\mathrm{[Bq/kg]}\f$.
     * \param compIdx The index of the component to consider
     */
    static Scalar specificActivity(int compIdx)
    {
        return log(2) / halfLife(compIdx) * 6.02214076e23 / molarMass(compIdx);
    }

    /*!
     * \brief Calculation of decay rate of a component
     * Used is the usual radioactive decay equation N = N*exp(-t/lambda).
     * \param compIdx The index of the component to consider
     */
    static Scalar decayRate(int compIdx)
    {
        return log(2)/halfLife(compIdx);
    }

    //! Binary diffusion coefficient
    //! (might depend on spatial parameters like pressure / temperature)
    static Scalar binaryDiffusionCoefficient(unsigned int compIdx,
                                             const Problem& problem,
                                             const Element& element,
                                             const SubControlVolume& scv)
    {
        static const Scalar D = getParam<Scalar>("Problem.BinaryDiffusionCoefficient");
        return D;
    }
};

} // end namespace FluidSystems
} // end namespace Dumux

#endif
